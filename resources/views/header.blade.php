<div class="container-fluid">
	<div class="row header pt30">
		<div class="col col-md-2 col-lg-3 d-none d-md-block"><img class="img-logo" src="{{ asset('assets/site/images/logo.png')}}" alt=""></div>
		<div class="col-3 d-block d-sm-none"><a href="{{route('home')}}"><img class="img-fluid" src="{{ asset('assets/site/images/logo.png')}}"></a></div>
		<div class="col-9 col-md-7 col-lg-6 justify-content-end">
			<nav class="navbar navbar-expand-md navbar-light justify-content-end">
				  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				    <span class="navbar-toggler-icon"></span>
				  </button>

				  <div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
				    <ul class="navbar-nav">
				      <li class="nav-item active">
				        <a class="nav-link {{ Route::is('home') ? 'font-weight-bold' : '' }}" href="{{route('home')}}">Home</a>
				      </li>
				      <li class="nav-item">
				        <a class="nav-link {{ Route::is('conheca') ? 'font-weight-bold' : '' }}" href="{{route('conheca')}}">Conheça</a>
				      </li>
				      <li class="nav-item">
				        <a class="nav-link {{ Route::is('servicos') ? 'font-weight-bold' : '' }}" href="{{route('servicos')}}">Serviços</a>
				      </li>
				      <li class="nav-item">
				        <a class="nav-link {{ Route::is('contato') ? 'font-weight-bold' : '' }}" href="{{route('contato')}}">Contato</a>
				      </li>
				      <div class="d-block d-sm-none">
				      
					      @if(Auth::guard('loja')->check()) 
						      	<li class="nav-item">
						        	<a class="nav-link {{ Route::is('painel.dash') ? 'font-weight-bold' : '' }}" href="{{route('painel.dash')}}">Minha conta</a>
						      	</li>
						      	<li class="nav-item">
						        	<a class="nav-link {{ Route::is('loja.logout') ? 'font-weight-bold' : '' }}" href="{{route('loja.logout')}}">Sair</a>
						      	</li>

						  @else
						  		<li class="nav-item">
						        	<a class="nav-link {{ Route::is('painel.dash') ? 'font-weight-bold' : '' }}" href="{{route('painel.dash')}}">Login</a>
						      	</li>
						      	<li class="nav-item">
						        	<a class="nav-link {{ Route::is('cadastro') ? 'font-weight-bold' : '' }}" href="{{route('cadastro')}}">Cadastro</a>
						      	</li>
					      @endif

					    </div>
				    </ul>
				  </div>
			</nav>
		</div>
		<div class="col col-md-3 col-lg-3 d-none d-md-block">

			@if( !empty( session('message') ) ) <div class="erro-login" data-login="erro" style="display:none;"></div> @endif
			
				@if(Auth::guard('loja')->check()) 

					<?php

						if (session()->get('carrinho') != NULL ) {
					?>

						<div class="carrinho-icon">
							<a href="{{route('carrinho.painel')}}" title="Ir para o Carrinho"><i class="fas fa-shopping-cart"></i> <?php $quant= count(session()->get('carrinho')); echo ($quant >0 )? "<span class='icon-quantidade'>".$quant."</span>" : "" ; ?></a>
						</div>

					<?php }else{ ?>

						<div class="carrinho-icon">
							<i class="fas fa-shopping-cart"></i>
						</div>

					<?php } ?>

					<div class="avatar-perfil">
						<img width="50" src="{{$auth->image ? $auth->image : 'https://via.placeholder.com/50x50'}}" alt="Imagem Perfil">
					</div>
					<div class="dropdown show pt20">
					  <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					   Olá {{$auth->name}}
					  </a>

					  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
					    <a class="dropdown-item" href="{{route('painel.dash')}}">Minha conta</a>
					    <a class="dropdown-item" href="{{route('loja.logout')}}">Sair</a>
					  </div>
					</div> 
				@else
					<a href="#" class="link_login" data-toggle="modal" <?php if( request()->route()->getName() != 'loja.login'){ echo 'data-target="#modal-login"'; } ?> ><i class="fas fa-lock pt20"></i> Entrar ou Cadastrar</a>
				@endif
		

		</div>
	</div>
</div>