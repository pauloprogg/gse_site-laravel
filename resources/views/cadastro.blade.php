@extends('layouts.app')

@section('content')


<div class="container">

    <!-- Cabeçalho pagina -->
    <div class="row pt90">
        <div class="col-12">
            <h1 class="playfair-h1-g">Faça seu cadastro!</h1>
        </div>
    </div>

    @include('cms.includes.error_messages')

    <!-- Form cadastro -->
    <div class="row pt90 box-cadastro pl30 pr30">

        <div class="col-12">
            <form action="{{route('userloja.cadastro')}}" method="POST">
                {{ csrf_field() }}

                <div class="row">

                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nome</label>
                            <input type="text" class="form-control" id="name" name="name" required placeholder="Nome completo">
                        </div>
                    </div>

                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">E-mail</label>
                            <input type="email" class="form-control" id="email" name="email" required placeholder="Insira o E-mail">
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Telefone <small class="text-muted">(Opcional)</small></label>
                            <input type="text" class="form-control telefone-mask" id="telefone" name="telefone" placeholder="Telefone Fixo">
                        </div>
                    </div>

                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Celular</label>
                            <input type="text" class="form-control celular-mask" id="celular" name="celular" required placeholder="Insira o E-mail">
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-6"> <!-- <image> do perfil do usuário -->
                        <div class="row">
                            <div class="col-12 no-pad">
                                <label for="email">Data nascimento</label>
                            </div>
                            <div class="col-12 col-md-4 col-lg-4 pb5">
                                <select class="form-control" id="dia" required name="dia_nasc"></select>
                            </div>
                            <div class="col-12 col-md-4 col-lg-4 pb5">
                                <select class="form-control" id="mes" required name="mes_nasc"></select>
                            </div>
                            <div class="col-12 col-md-4 col-lg-4">
                                <select class="form-control" id="ano" required name="ano_nasc"></select>
                            </div>
                        </div>
                        <input type="hidden" class="datanascimento" name="datanascimento" value="" required>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-12">
                                <label for="sexo">Sexo</label>
                            </div>
                            <div class="col-12">
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="radio" name="sexo" id="masculino" checked value="1">
                                  <label class="form-check-label" for="masculino">M</label>
                                </div>
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="radio" name="sexo" id="feminino" value="2">
                                  <label class="form-check-label" for="feminino">F</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                 <div class="row pt10">
                    <div class="col-md-6"> <!-- <password> do usuário -->
                        <div class="form-group">
                            <label for="password">Senha</label>
                            <input class="form-control" type="password" required name="password" />
                        </div>
                    </div>
                
                    <div class="col-md-6"> <!-- <password_confirmation> do usuário -->
                        <div class="form-group">
                            <label for="password_confirmation">Confirmar senha</label>
                            <input class="form-control" type="password" required name="password_confirmation" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 pl30">
                        <input class="form-check-input" type="checkbox" name="politicas" required>
                        <label class="form-check-label">
                          Li e concordo com todas as <a class="politicas" href="{{route('politicasdeprivacidade')}}">Políticas de Privacidade</a>
                        </label>
                    </div>
                </div>

                <div class="row pt30">
                    <div class="col-md-4"></div>
                    <div class="col-12 col-md-4">
                        <button class="btn-verde" type="submit" style="width: 100%;">Cadastrar-se</button>
                    </div>
                    <div class="col-md-4"></div>
                </div>

            </form>
        </div>


    </div>

</div>


@endsection
