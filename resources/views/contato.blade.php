@extends('layouts.app')

@section('content')

<!-- Sessão banner topo -->
<div class="container-fluid no-pad">

    <!-- Session Banner hide only xs-->
    <div class="row pl90 d-none d-sm-block pb50">
        <div class="col-12 banner-contato" style="background-image: url({{ asset($page->headimg)}});">
            <div class="chamada playfair-h1-w">
                {{$page->headtitle}}
            </div>
           <!--  <div class="fiquepordentro">
                Fique por dentro <span class="linha"></span> <i class="fab fa-facebook-f"></i> <i class="fab fa-instagram"></i>
            </div> -->
        </div>
    </div>

    <!-- Session Banner visible only xs-->
    <div class="row d-block d-sm-none pb90">
        <div class="col-12 banner-contato" style="background-image: url({{ asset($page->headimg)}});">
            <div class="chamada playfair-h1-w">
                {{$page->headtitle}}
            </div>
            <!-- <div class="fiquepordentro pl20">
                Fique por dentro <span class="linha"></span> <i class="fab fa-facebook-f"></i> <i class="fab fa-instagram"></i>
            </div> -->
        </div>
    </div>

</div>

<div class="container conteudo-contato">
    
    <div class="row">
        <div class="col-12 col-md-4">
            <ul>
                <li><i class="fas fa-map-marker-alt"></i> {{$info->endereco}}</li>
                @if ($info->telefone2 != '')
                <li><i class="fas fa-phone"></i> {{$info->telefone2}}</li>
                @endif
                <li><i class="fab fa-whatsapp"></i> {{$info->telefone1}}</li>
                <li><i class="far fa-envelope"></i> {{$info->email}}</li>
                
            </ul>
        </div>
        <div class="col-12 col-md-8">

            @if($errors->any())
                <div class="alert alert-danger">
                    <ul class="mb-0">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if($mensagem_contato != '')
                <div class="alert alert-success" role="alert">
                    {{$mensagem_contato}}
                </div>
            @endif
            
            <form class="form-contato" action="{{route('contato.enviar')}}" method="POST">
                {{ csrf_field() }}

                <div class="row">

                    <div class="col-12">
                        <div class="form-group">
                            <label for="nome">Nome</label>
                            <input type="text" class="form-control" id="name" name="name" required>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" class="form-control" id="email" name="email" required>
                        </div>
                    </div>

                     <div class="col-12">
                        <div class="form-group">
                            <label for="assunto">Assunto</label>
                            <input type="text" class="form-control" id="assunto" name="assunto" required>
                        </div>
                    </div>

                     <div class="col-12">
                        <div class="form-group">
                            <label for="assunto">Mensagem</label>
                            <textarea class="form-control" name="mensagem" rows="10" required></textarea>
                        </div>
                    </div>

                    <div class="col-12 d-flex justify-content-center">
                        <button class="btn-verde" type="submit">Enviar</button>
                    </div>

                </div>

            </form>

        </div>
        
    </div>

</div>
<div class="container-fluid no-pad">
    <!-- Session Conheça -->
    <section class="session-conheca">
        <div class="row pt90 pb100">
            <div class="col-12 pb65">
                <h1 class="playfair-h1-g">Conheça</h1>
            </div>
            <div class="col-md-6 col-lg-5 d-none d-md-block pl90">
                <img class="img-fluid" src="{{$conheca->destaquehomeimg}}">
            </div>
            <div class="col-12 col-md-6 col-lg-6 pb40">
                <h2 class="playfair-h2-b title-conheca">{{$conheca->summary}}</h2>
                <div class="row pt30 subtitle-conheca">
                    <div class="col-3 col-md-4">
                        <span class="linha-conheca"></span>
                    </div>
                    <div class="col-9 col-md-8 sumario">
                        {!!$conheca->content!!}
                        <a href="{{route('conheca')}}" class="btn-branco mr50 d-none d-md-block">Conheça Mais</a>
                    </div>
                </div>
                <div class="col-12 d-sm-block d-md-none">
                    <a href="{{route('conheca')}}" class="btn-branco">Conheça Mais</a>
                </div>
            </div>
            
            <div class="col-12 d-sm-block d-md-none">
                <img class="img-fluid" src="{{$conheca->destaquehomeimg}}">
            </div>
        </div>
    </section>
</div>

@endsection
