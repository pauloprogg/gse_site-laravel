@extends('layouts.cms_app')

@section('content')
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <h4 class="page-title">{{$title}}</h4>
                        <!-- breadcrumb -->
		                <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Painel</a></li>
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Serviços</a></li>
                            <li class="breadcrumb-item active">{{$title}}</li>
		                </ol>
		                <!-- breadcrumb -->
                    </div>
                </div>
            </div> <!-- end row -->
            
            <!-- Verifica e mostra erros dos campos obrigatórios -->
            @include('cms.includes.error_messages')

            <div class="row">
                <!-- Verifica e mostra mensagem de sucesso -->
                @include('cms.includes.alert_messages')
                <div class="col-12">
                    <div class="card m-b-20">
	                    <div class="card-body">
	                    	<form action="{{route('posts.store')}}" method="POST" enctype="multipart/form-data">
	                    		{{ csrf_field() }}
                                
                                <div class="form-group row">
                                    <div class="col-md-6">  <!-- <status> do post -->
                                        <label for="status">Salvar como</label>
                                        <select name="status" class="form-control">
                                            <option value="0">Publicado</option>
                                            <option value="1">Rascunho</option>
                                        </select>
                                    </div>
                                </div>

                                <hr>
                                
                                <div class="form-group row">
                                    <div class="col-md-6">  <!-- <categoria_id> do post -->
                                        <label for="categoria_id">Categoria</label>
                                        <select name="categoria_id" class="form-control" required>
                                            <option value="">Selecione a categoria</option>
                                            @foreach($categorias as $categoria)
                                            <option value="{{$categoria->id}}">{{$categoria->label}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-md-6"> <!-- <numero_profissional> do produto -->
                                      <div class="row">
                                          <div class="col-12">
                                              <label for="sexo">N° de Profissional no atendimento</label>
                                          </div>
                                          <div class="col-12">
                                              <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="numero_profissional" id="profissional1" checked value="1">
                                                <label class="form-check-label" for="masculino">1</label>
                                              </div>
                                              <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="numero_profissional" id="profissional2" value="2">
                                                <label class="form-check-label" for="feminino">2</label>
                                              </div>
                                              <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="numero_profissional" id="profissional3" value="3">
                                                <label class="form-check-label" for="feminino">3</label>
                                              </div>
                                              <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="numero_profissional" id="profissional4" value="4">
                                                <label class="form-check-label" for="feminino">4</label>
                                              </div>
                                              <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="numero_profissional" id="profissional5" value="5">
                                                <label class="form-check-label" for="feminino">5</label>
                                              </div>
                                          </div>
                                      </div>
                                    </div>

                                   
                                </div>

		                        <div class="form-group row">
                                    <div class="col-md-6"> <!-- <title> do conteúdo -->
                                        <label for="title">Nome do serviço</label>
                                        <input class="form-control" type="text" name="title" required/>
                                    </div>

                                    <div class="col-md-6"> <!-- <price> do conteúdo -->
                                        <label for="price">Valor</label>
                                        <input class="form-control dinheiro" type="text" name="price" required/>
                                    </div>
		                        </div>

                            <hr>

                            <div class="form-group row">
                                <div class="col-12 col-md-6">  <!-- <tempo-atendimento> do post -->
                                    <label for="tempo-atendimento">Tempo de atendimento</label>
                                    <select name="tempo_atendimento" class="form-control tempo-atendimento" required>
                                        <option value="">Selecione um intervalo</option>
                                        <option value="30" data-tipohora="meia">30m</option>
                                        <option value="60" data-tipohora="inteira">1:00h</option>
                                        <option value="90" data-tipohora="meia">1:30h</option>
                                        <option value="120" data-tipohora="inteira">2:00h</option>
                                        <option value="150" data-tipohora="meia">2:30h</option>
                                    </select>
                                </div>
                                <div class="col-12 col-md-6">
                                    <label for="tempo-atendimento" style="width: 100%;">Dias de atendimento</label>
                                    
                                    <label class="check-label-dsemana form-check-label">Segunda
                                      <input type="checkbox" value="segunda" class="form-check-input diasemana-check" checked="checked" name="segunda">
                                      <span class="checkround"></span>
                                    </label>
                                    <label class="check-label-dsemana form-check-label">Terça
                                      <input type="checkbox" value="terca" class="form-check-input diasemana-check" checked="checked" name="terca">
                                      <span class="checkround"></span>
                                    </label>
                                    <label class="check-label-dsemana form-check-label">Quarta
                                      <input type="checkbox" value="quarta" class="form-check-input diasemana-check" checked="checked" name="quarta">
                                      <span class="checkround"></span>
                                    </label>
                                    <label class="check-label-dsemana form-check-label">Quinta
                                      <input type="checkbox" value="quinta" class="form-check-input diasemana-check" checked="checked" name="quinta">
                                      <span class="checkround"></span>
                                    </label>
                                     <label class="check-label-dsemana form-check-label">Sexta
                                      <input type="checkbox" value="sexta" class="form-check-input diasemana-check" checked="checked" name="sexta">
                                      <span class="checkround"></span>
                                    </label>
                                     <label class="check-label-dsemana form-check-label">Sabado
                                      <input type="checkbox" value="sabado" class="form-check-input diasemana-check" name="sabado">
                                      <span class="checkround"></span>
                                    </label>
                                    <label class="check-label-dsemana form-check-label">Domingo
                                      <input type="checkbox" value="domingo" class="form-check-input diasemana-check" name="domingo">
                                      <span class="checkround"></span>
                                    </label>

                                    <button class="definir-agenda btn btn-danger" type="button" disabled>Definir Agenda</button>
                                </div>
                            </div>

                            <div class="card text-center card-agenda">
                              <div class="card-header d-flex justify-content-left">
                                Defina os dias e horários em que o serviço estará disponivel.
                              </div>
                              <div class="card-body">
                                <h5 class="card-title">Agenda do Serviço</h5>

                                    <ul class="nav nav-tabs justify-content-center" id="dias-semana" role="tablist">
                                      <li class="nav-item">
                                        <a class="nav-link" id="segunda-tab" data-toggle="tab" href="#segunda" role="tab" aria-controls="segunda" aria-selected="false">Segunda</a>
                                      </li>
                                      <li class="nav-item">
                                        <a class="nav-link" id="terca-tab" data-toggle="tab" href="#terca" role="tab" aria-controls="terca" aria-selected="false">Terça</a>
                                      </li>
                                      <li class="nav-item">
                                        <a class="nav-link" id="quarta-tab" data-toggle="tab" href="#quarta" role="tab" aria-controls="quarta" aria-selected="false">Quarta</a>
                                      </li>
                                      <li class="nav-item">
                                        <a class="nav-link" id="quinta-tab" data-toggle="tab" href="#quinta" role="tab" aria-controls="quinta" aria-selected="false">Quinta</a>
                                      </li>
                                      <li class="nav-item">
                                        <a class="nav-link" id="sexta-tab" data-toggle="tab" href="#sexta" role="tab" aria-controls="sexta" aria-selected="false">Sexta</a>
                                      </li>
                                      <li class="nav-item">
                                        <a class="nav-link" id="sabado-tab" data-toggle="tab" href="#sabado" role="tab" aria-controls="sabado" aria-selected="false">Sabado</a>
                                      </li>
                                      <li class="nav-item">
                                        <a class="nav-link" id="domingo-tab" data-toggle="tab" href="#domingo" role="tab" aria-controls="domingo" aria-selected="true">Domingo</a>
                                      </li>
                                    </ul>

                                    <div class="tab-content" id="myTabContent">

                                      <div class="tab-pane fade" id="segunda" role="tabpanel" aria-labelledby="segunda-tab">
                                          <div class="row d-flex justify-content-center box-horario box-segunda">

                                          </div>
                                      </div>

                                      <div class="tab-pane fade" id="terca" role="tabpanel" aria-labelledby="terca-tab">
                                          <div class="row d-flex justify-content-center box-horario box-terca">

                                              

                                          </div>
                                      </div>

                                      <div class="tab-pane fade" id="quarta" role="tabpanel" aria-labelledby="quarta-tab">
                                          <div class="row d-flex justify-content-center box-horario box-quarta">

                                          </div>
                                      </div>

                                      <div class="tab-pane fade" id="quinta" role="tabpanel" aria-labelledby="quinta-tab">
                                          <div class="row d-flex justify-content-center box-horario box-quinta">

                                               

                                          </div>
                                      </div>

                                      <div class="tab-pane fade" id="sexta" role="tabpanel" aria-labelledby="sexta-tab">
                                          <div class="row d-flex justify-content-center box-horario box-sexta">

                                          </div>
                                      </div>

                                      <div class="tab-pane fade" id="sabado" role="tabpanel" aria-labelledby="sabado-tab">
                                          <div class="row d-flex justify-content-center box-horario box-sabado">

                                          </div>
                                      </div>

                                      <div class="tab-pane fade" id="domingo" role="tabpanel" aria-labelledby="domingo-tab">
                                          <div class="row d-flex justify-content-center box-horario box-domingo">

                                          </div>
                                      </div>

                                    </div>  

                                    <input type="hidden" id="agendamento" name="agendamento" value="" required>                                

                              </div>
                              
                            </div>

		                        <hr>

                                <div class="form-group row">
                                    <div class="col-md-12"> <!-- <image> principal do post -->
                                        <img src="https://via.placeholder.com/300x300" alt="" class="mb-3 img-servico" width="150" /><br>
                                        <small class="observacao badge badge-warning"> Obs: A imagem acima é somente um preview, clique em salvar alterações para salvar a imagem! </small><br><br>
                                        <label for="image">Imagem do Serviço <small class="badge badge-primary">Principal</small></label>
                                        <input type="file" name="image" class="form-control imagem" required/>
                                    </div>
                                </div>

                                <hr>

                                <div class="form-group row">
                                    <div class="col-md-12"> <!-- <summary> do post -->
                                        <label for="summary">Resumo</label>
                                        <textarea class="form-control" rows="5" name="summary"></textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-12"> <!-- <content> do post -->
                                        <label for="content">Conteúdo</label>
                                        <textarea id="elm1" name="content"></textarea>
                                    </div>
                                </div>
                                

		                        <hr>

		                        <div class="form-group row">
		                            <div class="col-md-6"> <!-- <meta> keywords do post -->
										                <label for="keywords">Meta: Keywords</label>
	                                	<input class="form-control" type="text" name="keywords" />
		                            </div>
		                            <div class="col-md-6"> <!-- <meta> description do post -->
										                <label for="description">Meta: Description</label>
	                                	<input class="form-control" type="text" name="description" />
		                            </div>
		                        </div>

		                        <hr>

		                        <div class="form-group row">
                                    <div class="col-md-12 text-right">
                                        <button class="btn btn-primary btn-lg"><i class="fas fa-save"></i> Salvar alterações</button>
                                        <a href="{{route('posts.index')}}" class="btn btn-danger btn-lg"><i class="fas fa-window-close"></i> Cancelar</a>
                                    </div>
		                        </div>
	                        </form>
	                    </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->        
        </div> <!-- container-fluid -->
    </div> <!-- content -->
</div>
@endsection

@section('scripts')
    @include('cms.includes.tinymce')
    <script type="text/javascript" charset="utf-8">
        
        //Função que exibe a imagem antes de subir o arquivo
        $('.observacao').hide();
         $(function () {
            $(".imagem").change(function () {
                readURL(this);
            });
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    //alert(e.target.result);
                    $('.img-servico').attr('src', e.target.result);
                    $('.observacao').show();
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
        //Função que exibe a imagem antes de subir o arquivo


        //Exibe conograma de agenda baseada no tempo de atendimento e dias
        $('.card-agenda').hide();//Esconde a agenda até ser clicado no botão


        $('.tempo-atendimento').on('click', function(){
            agendamento= {}; //Define objeto vazio para armazenar os horarios de agendamento
            $('#agendamento').removeAttr('value');
            valor_tempo= $(this).val();//Recupera o vaor do select do tempo de atendimento
            Arraydias_semana= ['segunda','terca','quarta','quinta','sexta','sabado','domingo'];
  
            apagaHorarios(Arraydias_semana);//Limpa as divs antes de gerar outra vez

            if(valor_tempo != ''){
              //alert($(this).children('option:selected').attr('data-tipohora'));
              $.each($( Arraydias_semana ), function( index, value ) {

                geraHorarios(valor_tempo, value);
                
              });
            }
              

            $('.diasemana-check').removeAttr("disabled");
            $('.card-agenda').hide();//Esconde a agenda até ser clicado no botão
            $('.tab-pane').removeClass('show active');
            $('.nav-link').removeClass('active');
            valor_tempo= $(this).val();//Recupera o vaor do select do tempo de atendimento
            if(valor_tempo != ''){
                $('.definir-agenda').removeAttr("disabled");//Se o valor for vazio não permite 
            }else{
                $('.definir-agenda').prop('disabled', true);
                $('.card-agenda').hide();//Esconde a agenda até ser clicado no botão
            }
        });

        //Função que ativa ao clicar no botão definir agenda
        $('.definir-agenda').on('click', function() {
            $('.diasemana-check').prop('disabled', true);//Desabilita os checkbox ao clicar no botão Definir agenda
            $('.card-agenda').show();//Mostra a agenda depois que clicou no botão "Definir agenda" 
            $('.tab-pane').addClass('tab-oculta');

            //Roda um loop em cima dos checkbox para exibir/ocultas as tabs e gerar a array com os horarios dos dias
            $.each($( ".diasemana-check" ), function( index, value ) {
                divtab= '#'+$(this).val()+'-tab';
                iddiv= '#'+$(this).val();


                if ($(this).is(':checked')) {
                  var diasemanacheck= $(this).val();//captura o dia da semana selecionado no checkbox
                  console.log(diasemanacheck);
                  var sigla_dia= diasemanacheck.slice(0,3);//Monta sigla com as 3 primeiras letras do dia da semana

                  agendamento[sigla_dia]= new Array();//Cria um array vazio para todos os dias da semana clicados

                  if(valor_tempo != ''){
                      //Roda o loop dentro de cada dia da semana selecionada e preenche os valores de horario
                      $.each($( "#"+diasemanacheck+' .horario-check' ), function( index, value ) {
                        horariosMarcados('adicionar', $(this).attr('id') );//Passa o valor do campo hora
                      });
                  }

                }else{
                  console.log(divtab);
                  $(divtab).removeClass('show active').hide();
                }

            });

            //Roda loop nos dias da semana para ativar a primeira tab e para o loop
            $.each($( ".diasemana-check" ), function( index, value ) {
                divtab= '#'+$(this).val()+'-tab';
                iddiv= '#'+$(this).val();
                if ($(this).is(':checked')) {
                    $(divtab).addClass('active').trigger( "click" );
                    $(iddiv).addClass('show active');
                    return false;
                }
            });

        });

        // Ativa a Tab do bootstrap clicada
        // $('#dias-semana a').on('click', function (e) {
        //   e.preventDefault()
        //   $(this).tab('show')
        // })


        $("a.nav-link").on('click',function(){
          $(".tab-pane").hide().removeClass('show');
          $($(this).attr("href")).show().addClass('show');
        });


        //Função para trocar o icone e cor dos botões de hora ao clicar
        $(document).on('click', '.btn-horario', function(){
         
            var iddiv= $(this).attr('id');
            var acao= ($('#'+iddiv+' .fas').hasClass('fa-check'))? 'remover' : 'adicionar' ;
            $('#'+iddiv).toggleClass('horario-check horario-uncheck');
            $('#'+iddiv+' .fas').toggleClass('fa-check fa-times');

            horariosMarcados(acao, iddiv);//Envia o id do botão clicado e a ação "Remover ou adicionar"

        })

        //Função que grava em uma array os dias e horas que serão permitido o atendimento
        function horariosMarcados(acao, id ){

          var obj_nome= id.split('-')[0];//Recupera a sigla da semana

          if(acao == 'adicionar'){

            //Adciona horario do dia clicado ou grado no carregamento após clique do botão definir agenda
            agendamento[obj_nome].push(id);

          }else{

            //Caso a açã seja remover, recupera a posição do index com base do id
            var posicao = agendamento[obj_nome].indexOf(id);

            if ( posicao > -1) {

              agendamento[obj_nome].splice(posicao, 1);//Remove o valor da array

            }
            
          }

          agendaJason= JSON.stringify(agendamento);

          $('#agendamento').val(agendaJason);

        }

        function apagaHorarios(Arraydias_semana){
          $.each($( Arraydias_semana ), function( index, value ) {
            $('.box-'+value).empty();
          });
        }


        //Função que gera horario de acordo com o intervalo preenchendo os dia da semana
        function geraHorarios(tempo, diasdasemana){
            var totalminutos= 660; //Tempo de atendimento definido das 09 ás 20
            var divisao_horario= totalminutos/tempo|0; //Dividimos o tempo de atendimento para saber os intervalos
            var hora_inicio= 9;//Define horario inicial
            var minuto_inicio = 0;
            var intervalo= tempo/60;
            var intervalo_inteiro= parseInt(intervalo);
            var minutos = (tempo/60 - parseInt(intervalo)) * 60;


            for (var i = 0; i <= divisao_horario; i++) {

              if (minuto_inicio == 0) {
                var printminuto = '00';
              } else {
                var printminuto = minuto_inicio.toString();
              }

              var sigla= diasdasemana.substring(0, 3);
              $('.box-'+diasdasemana).append('<div class="col-1 btn-hora"><button id="'+sigla+'-'+hora_inicio+printminuto+'" type="button" class="btn-horario horario-check btn-verde"><i class="fas fa-check"></i> '+hora_inicio+':'+printminuto+'</button></div>');

              minuto_inicio += minutos;
              hora_inicio += intervalo_inteiro;
              if (minuto_inicio == 60) {
                hora_inicio += 1;
                minuto_inicio = 0;
              }
            }
        }

    </script>
@endsection