@extends('layouts.cms_app')

@section('content')
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <h4 class="page-title">{{$title}}</h4>
                        <!-- breadcrumb -->
		                <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Painel</a></li>
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Exceções</a></li>
                            <li class="breadcrumb-item active">{{$title}}</li>
		                </ol>
		                <!-- breadcrumb -->
                    </div>
                </div>
            </div> <!-- end row -->
            
            <!-- Verifica e mostra erros dos campos obrigatórios -->
            @include('cms.includes.error_messages')

            <div class="row">
                <!-- Verifica e mostra mensagem de sucesso -->
                @include('cms.includes.alert_messages')
                <div class="col-3"></div>
                <div class="col-6">
                    <div class="card m-b-20">
	                    <div class="card-body">
	                    	<form action="{{route('excecoes.store')}}" method="POST" enctype="multipart/form-data">
	                    		{{ csrf_field() }}

                                <div class="form-group row">
                                    <div class="col-md-12">  <!-- <post_id> do post -->
                                        <label for="post_id">Serviço</label>
                                        <select name="post_id" class="form-control" required>
                                            <option value="">Selecione a serviço</option>
                                            @foreach($servicos as $servico)
                                            <option value="{{$servico->id}}">{{$servico->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
	                    		
	                    		<div class="form-group row">
		                            <div class="col-md-12"> <!-- <label> do conteúdo -->
										<label for="label">Data da exceção</label>
	                                	<input class="form-control dataexcecao" type="text" placeholder="Exemplo: 01/01/2019" name="data" required/>
		                            </div>
		                        </div>

                                <div class="form-group row">
                                    <div class="col-md-12"> <!-- <label> do conteúdo -->
                                        <label for="label">Observação</label>
                                        <input class="form-control" type="text" name="content" />
                                    </div>
                                </div>

		                        <div class="form-group row">
                                    <div class="col-md-12 text-right">
                                        <button class="btn btn-primary btn-lg"><i class="fas fa-save"></i> Salvar alterações</button>
                                        <a href="{{route('excecoes.index')}}" class="btn btn-danger btn-lg"><i class="fas fa-window-close"></i> Cancelar</a>
                                    </div>
		                        </div>
	                        </form>
	                    </div>
                    </div>
                </div> <!-- end col-6 -->
                <div class="col-3"></div>
            </div> <!-- end row -->        
        </div> <!-- container-fluid -->
    </div> <!-- content -->
</div>
@endsection

@section('scripts')
    @include('cms.includes.tinymce')
    <script type="text/javascript" charset="utf-8">
        
        //Função que exibe a imagem antes de subir o arquivo
         $(function () {
            $(".perfilimg").change(function () {
                readURL(this);
            });
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    //alert(e.target.result);
                    $('.img-avatar').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
        //Função que exibe a imagem antes de subir o arquivo

    </script>
@endsection