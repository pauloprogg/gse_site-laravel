@extends('layouts.cms_app')

@section('content')

<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <h4 class="page-title">{{$title}}</h4>
                        <!-- breadcrumb -->
		                <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Painel</a></li>
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Usuários Loja</a></li>
                            <li class="breadcrumb-item active">{{$usuario->name}}</li>
		                </ol>
		                <!-- breadcrumb -->
                    </div>
                </div>
            </div> <!-- end row -->
            
            <!-- Verifica e mostra erros dos campos obrigatórios -->
            @include('cms.includes.error_messages')

            <div class="row">
                <!-- Verifica e mostra mensagem de sucesso -->
                @include('cms.includes.alert_messages')
                <div class="col-3"></div>
                <div class="col-6">
                    <div class="card m-b-20">
	                    <div class="card-body">
	                    	<form action="{{route('usuariosloja.update', $usuario->id)}}" method="POST" enctype="multipart/form-data">
	                    		{{ csrf_field() }}
                                <input type="hidden" name="_method" value="PUT" />
                                 <div class="form-group row">
                                    <div class="col-md-12">
                                        <label for="status">Status {{$usuario->status}}</label>
                                        <select class="form-control" id="status">
                                          <option value="1" @if($usuario->status == 1) selected @endif>Ativo</option>
                                          <option value="0" @if($usuario->status == 0) selected @endif>Inativo</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <div class="col-md-12"> <!-- <name> do usuário -->
                                        <label for="name">Nome</label>
                                        <input class="form-control" type="text" name="name" value="{{$usuario->name}}" required />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-12"> <!-- <email> do usuário -->
                                        <label for="email">E-mail</label>
                                        <input class="form-control" type="text" name="email" value="{{$usuario->email}}" required />
                                    </div>
                                </div>

                                 <div class="form-group row">
                                    <div class="col-md-6"> <!-- <telefone> do usuário -->
                                        <label for="telefone">Telefone</label>
                                        <input class="form-control" type="text" name="telefone" value="{{$usuario->telefone}}" />
                                    </div>
                                    <div class="col-md-6"> <!-- <celular> do usuário -->
                                        <label for="celular">Celular</label>
                                        <input class="form-control" type="text" name="celular" value="{{$usuario->celular}}" required />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-12"> <!-- <image> do perfil do usuário -->
                                        <div class="row">
                                            <div class="col-12">
                                                <label for="email">Data nascimento</label>
                                            </div>
                                            
                                            <div class="col-4">
                                                <select class="form-control" id="dia" required name="dia_nasc"></select>
                                            </div>
                                            <div class="col-4">
                                                <select class="form-control" id="mes" required name="mes_nasc"></select>
                                            </div>
                                            <div class="col-4">
                                                <select class="form-control" id="ano" required name="ano_nasc"></select>
                                            </div>
                                        </div>
                                        <div style="display:none;" class="datanascimento_db" data-dia="{{$datanascimentoArray[2]}}" data-mes="{{$datanascimentoArray[1]}}" data-ano="{{$datanascimentoArray[0]}}"></div>
                                        <input type="hidden" class="datanascimento" name="datanascimento" value="{{$usuario->datanascimento}}" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-12"> <!-- <sexo> do perfil do usuário -->
                                        <div class="row">
                                            <div class="col-12">
                                                <label for="sexo">Sexo</label>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="sexo" id="masculino" @if($usuario->status == 1) checked @endif value="1">
                                                  <label class="form-check-label" for="masculino">M</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="sexo" id="feminino" @if($usuario->status == 2) checked @endif value="2">
                                                  <label class="form-check-label" for="feminino">F</label>
                                                </div>
                                            </div>
                                        </div>
                                             
                                    </div>
                                </div>

                                <hr>

                                <div class="form-group row">
                                    <div class="col-md-12"> <!-- <image> do perfil do usuário -->
                                        <img src="{{$usuario->image ? $usuario->image : 'https://via.placeholder.com/600x600'}}" alt="" class="mb-3 img-avatar" width="100" /><br>
                                        <label for="image">Foto</label>
                                        <input type="file" name="image" class="form-control image" />
                                    </div>
                                </div>

                                <hr>

                                <div class="form-group row">
                                    <div class="col-md-12"> <!-- <password> do usuário -->
                                        <label for="password">Senha</label>
                                        <input class="form-control" type="password" name="password" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-12"> <!-- <password_confirmation> do usuário -->
                                        <label for="password_confirmation">Confirmar senha</label>
                                        <input class="form-control" type="password" name="password_confirmation" />
                                    </div>
                                </div>
                                

                                <hr>

                                <div class="form-group row">
                                    <div class="col-md-12 text-right">
                                        <button class="btn btn-primary btn-lg"><i class="fas fa-save"></i> Salvar alterações</button>
                                        <a href="{{route('usuariosloja.index')}}" class="btn btn-danger btn-lg"><i class="fas fa-window-close"></i> Cancelar</a>
                                    </div>
                                </div>
	                        </form>
	                    </div>
                    </div>
                </div> <!-- end col -->
                <div class="col-3"></div>
            </div> <!-- end row -->        
        </div> <!-- container-fluid -->
    </div> <!-- content -->
</div>
@endsection

@section('scripts')
    @include('cms.includes.tinymce')
    <script type="text/javascript" charset="utf-8" >
        
        $(document).ready(function() {

            var dia_db= $('.datanascimento_db').data('dia');
            var mes_db= $('.datanascimento_db').data('mes');
            var ano_db= $('.datanascimento_db').data('ano');
            

            $.dobPicker({
                daySelector: '#dia', /* Required */
                monthSelector: '#mes', /* Required */
                yearSelector: '#ano', /* Required */
                dayDefault: 'Dia', /* Optional */
                monthDefault: 'Mês', /* Optional */
                yearDefault: 'Ano', /* Optional */
                minimumAge: 18, /* Optional */
                maximumAge: 90 /* Optional */
            });

            var mesdoano = ["Mês","Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];

            $( "#mes option" ).each(function( index ) {
              $( this ).text(mesdoano[index]);
            });


            $('#dia option[value="'+dia_db+'"]').attr("selected", true);
            $('#mes option[value="'+mes_db+'"]').attr("selected", true);
            $('#ano option[value="'+ano_db+'"]').attr("selected", true);

            $('#dia').on('change', function() {
                dia= this.value;
                $('.datanascimento').val('');
            });

            $('#mes').on('change', function() {
                mes= this.value;
                $('.datanascimento').val('');
            });

            $('#ano').on('change', function() {
                ano= this.value;
                datanascimento= ano+'-'+mes+'-'+dia;
                $('.datanascimento').val(datanascimento);
            });

            //Função que exibe a imagem antes de subir o arquivo
             $(function () {
                $(".image").change(function () {
                    readURL(this);
                });
            });
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        //alert(e.target.result);
                        $('.img-avatar').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }
            //Função que exibe a imagem antes de subir o arquivo

        });

    </script>
@endsection