@extends('layouts.cms_app')

@section('content')
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <h4 class="page-title">{{$title}}</h4>
                        <!-- breadcrumb -->
		                <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Painel</a></li>
                            <li class="breadcrumb-item active">Informações</li>
		                </ol>
		                <!-- breadcrumb -->
                    </div>
                </div>
            </div> <!-- end row -->
            
            <!-- Verifica e mostra erros dos campos obrigatórios -->
            @include('cms.includes.error_messages')

            <div class="row">
                <!-- Verifica e mostra mensagem de sucesso -->
                @include('cms.includes.alert_messages')
                <div class="col-12">
                    <div class="card m-b-20">
	                    <div class="card-body">
	                    	<form action="{{route('informacoes.update', $info->id)}}" method="POST">
	                    		{{ csrf_field() }}
                                <input type="hidden" name="_method" value="PUT" />

                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs nav-tabs-custom" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#tab1" role="tab">Contato</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tab2" role="tab">Outros</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tab3" role="tab">Redes Sociais</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tab4" role="tab">Scripts</a>
                                    </li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane active p-3" id="tab1" role="tabpanel">
                                        <div class="form-group row">
                                            <div class="col-md-6"> <!-- <telefone1> -->
                                                <label for="telefone1">Telefone</label>
                                                <input class="form-control" type="text" name="telefone1" value="{{$info->telefone1}}" />
                                            </div>
                                            <div class="col-md-6"> <!-- <telefone2> -->
                                                <label for="telefone2">Telefone</label>
                                                <input class="form-control" type="text" name="telefone2" value="{{$info->telefone2}}" />
                                            </div>
                                            <div class="col-md-12 pt-3"> <!-- <email> -->
                                                <label for="email">E-mail</label>
                                                <input class="form-control" type="text" name="email" value="{{$info->email}}" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane p-3" id="tab2" role="tabpanel">
                                        <div class="form-group row">
                                            <div class="col-md-12"> <!-- <endereco> -->
                                                <label for="endereco">Endereço</label>
                                                <input class="form-control" type="text" name="endereco" value="{{$info->endereco}}" />
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-12"> <!-- <footer> -->
                                                <label for="footer">Texto do rodapé</label>
                                                <textarea class="form-control" name="footer" rows="10">{{$info->footer}}</textarea>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-12"> <!-- <copyright> -->
                                                <label for="copyright">Copyright</label>
                                                <input class="form-control" type="text" name="copyright" value="{{$info->copyright}}" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane p-3" id="tab3" role="tabpanel">
                                        <div class="form-group row">
                                            <div class="col-md-6"> <!-- <facebook> -->
                                                <label for="facebook">Facebook</label>
                                                <input class="form-control" type="text" name="facebook" value="{{$info->facebook}}" />
                                            </div>
                                            <div class="col-md-6"> <!-- <Instagram> -->
                                                <label for="instagram">Instagram</label>
                                                <input class="form-control" type="text" name="instagram" value="{{$info->instagram}}" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane p-3" id="tab4" role="tabpanel">
                                        <div class="form-group row">
                                            <div class="col-md-12"> <!-- <scriptshead> -->
                                                <label for="scriptshead">Scripts do Cabeçalho</label>
                                                <textarea name="scriptshead" class="form-control" cols="30" rows="7">{{$info->scriptshead}}</textarea>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-12"> <!-- <scriptsfoot> -->
                                                <label for="scriptsfoot">Scripts do Rodapé</label>
                                                <textarea name="scriptsfoot" class="form-control" cols="30" rows="7">{{$info->scriptsfoot}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

		                        <hr>

		                        <div class="form-group row">
                                    <div class="col-md-12 text-right">
                                        <button class="btn btn-primary btn-lg"><i class="fas fa-save"></i> Salvar alterações</button>
                                    </div>
		                        </div>
	                        </form>
	                    </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->        
        </div> <!-- container-fluid -->
    </div> <!-- content -->
</div>
@endsection