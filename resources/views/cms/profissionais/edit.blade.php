@extends('layouts.cms_app')

@section('content')
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <h4 class="page-title">{{$title}}</h4>
                        <!-- breadcrumb -->
		                <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Painel</a></li>
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Páginas</a></li>
                            <li class="breadcrumb-item active">{{$title}}</li>
		                </ol>
		                <!-- breadcrumb -->
                    </div>
                </div>
            </div> <!-- end row -->
            
            <!-- Verifica e mostra erros dos campos obrigatórios -->
            @include('cms.includes.error_messages')

            <div class="row">
                <!-- Verifica e mostra mensagem de sucesso -->
                @include('cms.includes.alert_messages')
                <div class="col-12">
                    <div class="card m-b-20">
	                    <div class="card-body">
	                    	<form action="{{route('profissionais.update', $profissional->id)}}" method="POST" enctype="multipart/form-data">
	                    		{{ csrf_field() }}
                                <input type="hidden" name="_method" value="PUT" />

		                        <div class="form-group row">
                                    <div class="col-md-12"> <!-- <label> do conteúdo -->
                                        <label for="label">Nome</label>
                                        <input class="form-control" type="text" name="nome" value="{{$profissional->nome}}" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-4"> <!-- <perfilimg> principal da página -->
                                        <img src="{{$profissional->perfilimg ? $profissional->perfilimg : 'https://via.placeholder.com/100x100'}}" alt="" class="mb-3 img-avatar" width="100" /><br>
                                        <label for="perfilimg">Imagem do <small class="badge badge-primary">Perfil</small></label>
                                        <input type="file" name="perfilimg" class="form-control perfilimg" />
                                    </div>
                                </div>

		                        <hr>

		                        <div class="form-group row">
                                    <div class="col-md-12 text-right">
                                        <button class="btn btn-primary btn-lg"><i class="fas fa-save"></i> Salvar alterações</button>
                                        <a href="{{route('categorias.index')}}" class="btn btn-danger btn-lg"><i class="fas fa-window-close"></i> Cancelar</a>
                                    </div>
		                        </div>
	                        </form>
	                    </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->        
        </div> <!-- container-fluid -->
    </div> <!-- content -->
</div>
@endsection


@section('scripts')
    <script type="text/javascript" charset="utf-8">
        
        //Função que exibe a imagem antes de subir o arquivo
         $(function () {
            $(".perfilimg").change(function () {
                readURL(this);
            });
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    //alert(e.target.result);
                    $('.img-avatar').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
        //Função que exibe a imagem antes de subir o arquivo

    </script>
@endsection