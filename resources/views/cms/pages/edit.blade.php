@extends('layouts.cms_app')

@section('content')

<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <h4 class="page-title">{{$title}}</h4>
                        <!-- breadcrumb -->
		                <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Painel</a></li>
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Páginas</a></li>
                            <li class="breadcrumb-item active">{{$page->name}}</li>
		                </ol>
		                <!-- breadcrumb -->
                    </div>
                </div>
            </div> <!-- end row -->
            
            <!-- Verifica e mostra erros dos campos obrigatórios -->
            @include('cms.includes.error_messages')

            <div class="row">
                <!-- Verifica e mostra mensagem de sucesso -->
                @include('cms.includes.alert_messages')
                <div class="col-12">
                    <div class="card m-b-20">
	                    <div class="card-body">
	                    	<form action="{{route('pages.update', $page->id)}}" method="POST" enctype="multipart/form-data">
	                    		{{ csrf_field() }}
                                <input type="hidden" name="_method" value="PUT" />
		                        <div class="form-group row">
		                            <div class="col-md-4"> <!-- <title> da página -->
										<label for="name">Nome da página</label>
	                                	<input class="form-control" type="text" name="name" value="{{$page->name}}" />
		                            </div>
		                            <div class="col-md-8"> <!-- <title> do conteúdo -->
										<label for="title">Título da página</label>
	                                	<input class="form-control" type="text" name="title" value="{{$page->title}}" />
		                            </div>
		                        </div>
                                @if($page->id == 2)
		                        <hr>
                                <div class="form-group row">
                                    <div class="col-md-12">  <!-- <galery> da página -->
                                        <label for="galeria_id">Galeria de imagens</label>
                                        <select name="galeria_id" class="form-control">
                                            <option value="">Nenhuma galeria selecionada</option>
                                            @foreach($galeria as $galeria)
                                                <option value="{{$galeria->id}}" @if($galeria->id == $page->galeria_id) selected @endif>{{$galeria->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @endif

                                <hr>

                                <div class="form-group row">
                                    <div class="col-md-4"> <!-- <headimg> principal da página -->
                                        <img src="{{$page->headimg ? $page->headimg : 'https://via.placeholder.com/1600x600'}}" alt="" class="mb-3" width="150" /><br>
                                        <label for="headimg">Imagem destaque <small class="badge badge-primary">Cabeçalho</small></label>
                                        <input type="file" name="headimg" class="form-control" />
                                    </div>
                                    <div class="col-md-8"> <!-- <headtitle> principal da página -->
                                        <label for="headtitle">Chamada da imagem <small class="badge badge-primary">Cabeçalho</small></label>
                                        <input type="text" name="headtitle" class="form-control" value="{{$page->headtitle}}" />
                                    </div>
                                </div>

                                @if($page->id == 2)
                                <hr>
                                    <div class="form-group row">
                                        <div class="col-md-12"> <!-- <destaquehomeimg> principal da página -->
                                            <img src="{{$page->destaquehomeimg ? $page->destaquehomeimg : 'https://via.placeholder.com/400x500'}}" alt="" class="mb-3" width="150" /><br>
                                            <label for="destaquehomeimg">Imagem Home <small class="badge badge-primary">Imagem home</small></label>
                                            <input type="file" name="destaquehomeimg" class="form-control" />
                                        </div>
                                    </div>
                                @endif

                                @if($page->id == 3)
                                <hr>
                                    <div class="form-group row">
                                        <div class="col-md-4"> <!-- <destaquehomeimg> principal da página -->
                                            <img src="{{$page->destaquehomeserv1 ? $page->destaquehomeserv1 : 'https://via.placeholder.com/400x500'}}" alt="" class="mb-3" width="150" /><br>
                                            <label for="destaquehomeserv1">Imagem Home 1 <small class="badge badge-primary">Imagem home</small></label>
                                            <input type="file" name="destaquehomeserv1" class="form-control" />
                                        </div>
                                        <div class="col-md-4"> <!-- <destaquehomeserv2> principal da página -->
                                            <img src="{{$page->destaquehomeserv2 ? $page->destaquehomeserv2 : 'https://via.placeholder.com/400x500'}}" alt="" class="mb-3" width="150" /><br>
                                            <label for="destaquehomeserv2">Imagem Home 2 <small class="badge badge-primary">Imagem home</small></label>
                                            <input type="file" name="destaquehomeserv2" class="form-control" />
                                        </div>
                                        <div class="col-md-4"> <!-- <destaquehomeserv3> principal da página -->
                                            <img src="{{$page->destaquehomeserv3 ? $page->destaquehomeserv3 : 'https://via.placeholder.com/400x500'}}" alt="" class="mb-3" width="150" /><br>
                                            <label for="destaquehomeserv3">Imagem Home 3 <small class="badge badge-primary">Imagem home</small></label>
                                            <input type="file" name="destaquehomeserv3" class="form-control" />
                                        </div>
                                    </div>
                                @endif
                                
                                @if($page->id == 2)
                                <hr>

                                <div class="form-group row">
                                    <div class="col-md-12"> <!-- <summary> da página -->
                                        <label for="summary">Introdução da página</label>
                                        <textarea name="summary" class="form-control" rows="5">{{$page->summary}}</textarea>
                                    </div>
                                </div>
                                @endif
                                
                                @if($page->id != 1)
                                <hr>

                                <div class="form-group row">
                                    <div class="col-md-12"> <!-- <content> da página -->
                                        <label for="content">Conteúdo</label>
                                        <textarea id="elm1" name="content">{{$page->content}}</textarea>
                                    </div>
                                </div>
                                @endif

		                        <hr>

		                        <div class="form-group row">
		                            <div class="col-md-6"> <!-- <meta> keywords da página -->
										<label for="keywords">Meta: Keywords</label>
	                                	<input class="form-control" type="text" name="keywords" value="{{$page->keywords}}" />
		                            </div>
		                            <div class="col-md-6"> <!-- <meta> description da página -->
										<label for="description">Meta: Description</label>
	                                	<input class="form-control" type="text" name="description" value="{{$page->description}}" />
		                            </div>
		                        </div>

		                        <hr>

		                        <div class="form-group row">
                                    <div class="col-md-12 text-right">
                                        <button class="btn btn-primary btn-lg"><i class="fas fa-save"></i> Salvar alterações</button>
                                        <a href="{{route('pages.index')}}" class="btn btn-danger btn-lg"><i class="fas fa-window-close"></i> Cancelar</a>
                                    </div>
		                        </div>
	                        </form>
	                    </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->        
        </div> <!-- container-fluid -->
    </div> <!-- content -->
</div>
@endsection

@section('scripts')
    @include('cms.includes.tinymce')
@endsection