@extends('layouts.cms_app')

@section('content')
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <h4 class="page-title">{{$title}}</h4>
                        <!-- breadcrumb -->
		                <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Painel</a></li>
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Pedidos</a></li>
                            <li class="breadcrumb-item active">{{$title}}</li>
		                </ol>
		                <!-- breadcrumb -->
                    </div>
                </div>
            </div> <!-- end row -->

            <div class="row">

                   <div class="col-lg-5 offset-lg-7 col-md-5 offset-md-7">
                       <form action="{{route('pedidos.busca', '')}}" method="POST" class="form-inline">
                           {{ csrf_field() }}

                           <div class="input-group" style="width:100%;">

                               <input value="{{$busca}}" type="text" class="form-control mb-2 mr-sm-2" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" name="busca">

                              <button type="submit" class="btn btn-info waves-effect waves-light mb-2 mr-sm-2"><i class="fas fa-search"></i></button>

                               @if( !empty( $busca ) )<a href="{{route('pedidos.index')}}" class="btn btn-primary mb-2"><i class="fas fas fa-times-circle"></i></a>@endif

                           </div>
                       </form>
                   </div>

           </div><!-- end row -->

            <div class="row">
                <!-- Verifica e mostra mensagem de sucesso -->
                @include('cms.includes.alert_messages')
                <div class="col-12">
                    <div class="card m-b-20">
                        <div class="card-body">
                            <table class="table table-striped mb-0">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>NOME DO SERVIÇO</th>
                                        <th>DATA</th>
                                        <th>HORÁRIO</th>
                                        <th>STATUS PG</th>
                                        <th>NOME USUÁRIO</th>
                                        <th>AÇÕES</th>
                                    </tr>
                                </thead>
                            
                                <tbody>
                                @if(count($pedidos)>0)
                                    @foreach($pedidos as $pedido)
                                    <tr>
                                        <th scope="row">{{$pedido->id}}</th>
                                        <td>{{$pedido->titulo_produto}}</td>
                                        <td>{{date("d/m/Y", strtotime($pedido->data_agendamento))}}</td>
                                        <td>{{$pedido->hora_agendamento}}</td>
                                        <td>
                                            @switch($pedido->statuspg)
                                                @case(0)
                                                    <span class="badge badge-secondary">Em processo</span>
                                                    @break

                                                @case(1)
                                                    <span class="badge badge-success">Pago</span>
                                                    @break

                                                @default
                                                    <span class="badge badge-danger">Cancelado</span>
                                            @endswitch
                                        </td>
                                        <td>
                                            {{$pedido->usuarioloja->name}}<br>
                                            {{$pedido->usuarioloja->email}}
                                        </td>
                                        <td>
                                                @if($auth->role_id == 1) <!-- 1 = Administrador | 2 = Cliente | 3 = Conteudoria | 4 = Editor | 5 = Redator -->
                                                <a target="_blank" href="{{route('usuariosloja.edit', $pedido->usuarioloja->id)}}" class="btn btn-primary"><i class="fas fa-edit"></i> Ver usuário</a>
                                                @endif
                                        </td>
                                        
                                    </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td>
                                           Não há categorias cadastradas!
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->
            <div class="row">
                <div class="col-md-12">
                    {{$pedidos->appends(Request::except('page','_token'))->links()}}
                </div>
            </div>
        </div> <!-- container-fluid -->
    </div> <!-- content -->
</div>
@endsection