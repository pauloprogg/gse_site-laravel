@extends('layouts.app')

@section('content')

@if($page->headtitle != '')

<div class="container-fluid no-pad">

    <!-- Session Banner hide only xs-->
    <div class="row pl90 d-none d-sm-block">
        <div class="col-12 banner-politicas" style="background-image: url({{ asset($page->headimg)}});">
            <div class="chamada playfair-h1-w">
                {{$page->headtitle}}
            </div>
            <div class="fiquepordentro">
                Fique por dentro <span class="linha"></span> <i class="fab fa-facebook-f"></i> <i class="fab fa-instagram"></i>
            </div>
        </div>
    </div>

    <!-- Session Banner visible only xs-->
    <div class="row d-block d-sm-none">
        <div class="col-12 banner-politicas" style="background-image: url({{ asset($page->headimg)}});">
            <div class="chamada playfair-h1-w">
                {{$page->headtitle}}
            </div>
            <div class="fiquepordentro pl20">
                Fique por dentro <span class="linha"></span> <i class="fab fa-facebook-f"></i> <i class="fab fa-instagram"></i>
            </div>
        </div>
    </div>

</div>

@endif

<div class="container">

	<!-- Cabeçalho pagina -->
    <div class="row pt90">
        <div class="col-12">
            <h1 class="playfair-h1-g">Política de Privacidade</h1>
        </div>
    </div>

	<div class=" row pt50">
		<div class="col-12">
			{!!$page->content!!}
		</div>
	</div>
	
</div>

@endsection
