<div class="container-fluid no-pad">
	<div class="row footer-top pt30">
		<div class="col-12 col-md-12 col-lg-3 offset-lg-1 pt50">
			<div class="row d-flex">
				<img src="{{ asset('assets/site/images/logo.png')}}" class="logo-footer" alt="Go Spa Express">
				<p class="pt30">
					{{$info->footer}}
				</p>
				<p>
					Empresa estabelecida em Sorocaba desde 2014.
				</p>
				
			</div>
		</div>
		<div class="col-12 col-md-12 col-lg-8 mapa-paginas pt50">
			<div class="row">
				<div class="col-12 col-md-2 col-lg-2">
					<ul>
						<li class="hd-list"><a href="{{route('home')}}">Home</a></li>
						<li><a href="{{route('conheca')}}">Conheça</a></li>
						<li><a href="{{route('servicos')}}">Serviços</a></li>
						<li><a href="{{route('contato')}}">Contato</a></li>
						<li><a href="#" data-toggle="modal" data-target="#modal-login">Entrar ou Cadastrar</a></li>
					</ul>
				</div>
				<div class="col-12 col-md-2 col-lg-2">
					<ul>
						<li class="hd-list">Políticas</li>
						<li><a href="{{route('politicasdeprivacidade')}}">Privacidade</a></li>
						<li><a href="{{route('compra')}}">Compra</a></li>
						<li><a href="{{route('cancelamento')}}">Cancelamento</a></li>
					</ul>
				</div>
				<div class="col-12 col-md-3 col-lg-2">

					
					<ul>
						<li class="hd-list">Serviços</li>
						@foreach($categorias as $cat)
							<li><a href="{{url('/servicos#'.$cat->slug)}}">{{$cat->label}}</a></li>
						@endforeach
						
					</ul>
				</div>
				<div class="col-12 col-md-5 col-lg-3">
					<ul>
						<li class="hd-list">Contato</li>
						<li><i class="fab fa-whatsapp"></i> {{$info->telefone1}}</li>
						@if ($info->telefone2 != '')
							<li><i class="fas fa-phone"></i> {{$info->telefone2}}</li>
						@endif
						<li><i class="far fa-envelope"></i> {{$info->email}}</li>
						<li><i class="fas fa-map-marker-alt"></i> {{$info->endereco}}</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="row footer pt30">
		<div class="col-12 col-md-6 col-lg-5 offset-lg-1 pt20 d-flex">
			<span>©<?php echo date('Y'); ?> {{$info->copyright}}</span>
		</div>
		<div class="col-12 col-md-6 col-lg-5 pt20 pr20 pb30 d-flex justify-content-end">
			<img src="{{ asset('assets/site/images/logo_sorocabacom_branco.png')}}" width="150" alt="Agência Sorocabacom">
		</div>
	</div>
</div>