@extends('layouts.app')

@section('content')

<div class="container-fluid no-pad">

    <!-- Session Banner hide only xs-->
    <div class="row pl90 d-none d-sm-block">
        <div class="col-12 banner-conheca" style="background-image: url({{ asset($page->headimg)}});">
            <div class="chamada playfair-h1-w">
                {{$page->headtitle}}
            </div>
            <div class="fiquepordentro">
                Fique por dentro <span class="linha"></span> <i class="fab fa-facebook-f"></i> <i class="fab fa-instagram"></i>
            </div>
        </div>
    </div>

    <!-- Session Banner visible only xs-->
    <div class="row d-block d-sm-none">
        <div class="col-12 banner-home" style="background-image: url({{ asset($page->headimg)}});">
            <div class="chamada playfair-h1-w">
                {{$page->headtitle}}
            </div>
            <div class="fiquepordentro pl20">
                Fique por dentro <span class="linha"></span> <i class="fab fa-facebook-f"></i> <i class="fab fa-instagram"></i>
            </div>
        </div>
    </div>

</div>

<div class="container">
	<!-- Session Introdução -->
    <section class="intro-conheca">
        <div class="row pt90 pb100">
            <div class="col-md-2 col-lg-2"></div>
            <div class="col-12 col-md-8 col-lg-8 pb40">
                <h2 class="playfair-h2-b title-conheca">{{$page->summary}}</h2>
                <div class="row pt30 subtitle-conheca">
                    <div class="col-3 col-md-4">
                        <span class="linha-conheca"></span>
                    </div>
                    <div class="col-9 col-md-8 sumario">
                        {!!$page->content!!}
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-lg-2"></div>
            
        </div>
    </section>
</div>


<div class="container galeria">
	<!-- Session Galeria -->
    <div class="row grid">
    	<div class="grid-sizer"></div>

    	<?php $aux=0;
        	foreach($galeria->medias as $imagem){
        ?>
        	<div class="grid-item-conheca pl15 pr15">
        		<img src="{{$imagem->file}}" class="img-fluid img-gal pb40 <?php if($aux==1){ echo 'pt90'; } ?>" alt="">
        	</div>
        	
        <?php    
        	$aux++;}
        ?>
    </div>
</div>

<div class="container-fluid no-pad">

    <!-- Session Serviços -->
    <section class="session-servicos">
        <div class="row pt40 pl20 pr20">

            <div class="col-md-2 col-lg-2"></div>

            <div class="col-12 col-md-5 col-lg-5 pb65">
                <h1 class="playfair-h1-g pb20">Serviços</h1>
                <p>{!!$servico->content!!}</p>
            </div>

            <div class="col-md-1 col-lg-1"></div>

            <div class="col-12 col-md-3 col-lg-3 pb50">
                <a href="{{route('conheca')}}" class="btn-escuro">Conheça Mais</a>
            </div>   
        </div>
        <div class="row no-mar">
            <div class="col-12 col-md-12 col-lg-4 img-serv pl30 pr30 pb50">
                <div class="div-img d-flex align-items-end justify-content-center" style="background-image: url( {{asset($servico->destaquehomeserv1)}} );">
                    <h1 class="playfair-h1-w">Spa dos Pés</h1>
                </div>
            </div>
            <div class="col-12 col-md-12 col-lg-4 img-serv pl30 pr30 pb50">
                <div class="div-img d-flex align-items-end justify-content-center" style="background-image: url( {{asset($servico->destaquehomeserv2)}} );">
                    <h1 class="playfair-h1-w">Depilação</h1>
                </div>
            </div>
            <div class="col-12 col-md-12 col-lg-4 img-serv pl30 pr30 pb50">
                <div class="div-img d-flex align-items-end justify-content-center" style="background-image: url( {{asset($servico->destaquehomeserv3)}} );">
                    <h1 class="playfair-h1-w">Spa das Mãos</h1>
                </div>
            </div>
        </div>
    </section>
        

</div>


@endsection

@section('scripts')

<script type="text/javascript">

        setTimeout(function(){ 

            //Usando o Isotope para formatar as imagens da galeria da pagina conhaeça
            var $grid = $('.grid').masonry({
              itemSelector: '.grid-item-conheca',
              percentPosition: true,
              columnWidth: '.grid-sizer'
            });
            // layout Masonry after each image loads
            $grid.imagesLoaded().progress( function() {
              $grid.masonry();
            }); 
             
        }, 5000);   

</script>

@endsection