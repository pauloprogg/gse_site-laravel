@component('mail::message')
# Olá, {{ $data['name'] }}!

Sua conta foi criada com sucesso!.
Sua senha de acesso é: {{ $data['password'] }}

@component('mail::button', ['url' => route('home')])
Acessar minha conta
@endcomponent


Veja nossas <a href="" target="_blank">Políticas e Privacidade</a>.

Atenciosamente,<br>
Equipe Go Spa Express
@endcomponent
