<?php 
	$aux1=0;
	foreach ($agendamento as $agenda => $value ) {

		//Define array com os dias da semana e com o valor numerico
		$dias_numero['dom']= 0;
		$dias_numero['seg']= 1; 
		$dias_numero['ter']= 2;
		$dias_numero['qua']= 3;
		$dias_numero['qui']= 4;
		$dias_numero['sex']= 5;
		$dias_numero['sab']= 6;

		//Armazena os dias da semana que o serviço estará disponivel
		echo "<div class='diasemana' data-numerodia='".$dias_numero[$agenda]."'></div>";
	$aux1++;}


	$execoes_db = json_decode(json_encode($servico->excecoes), true);

	$data_excecao= Array();
	foreach ($execoes_db as $excecao_data) {
		$excecao_data['data']= date('j-n-Y', strtotime($excecao_data['data']));
		$data_excecao[]= $excecao_data['data'];
	}


?>

<script>
	
	dias_excecao= new Array(<?php echo "'".implode("','", $data_excecao)."'" ?>);

</script>

@extends('layouts.app')

@section('content')

<div class="page-serv">
	
	<div class="container-fluid no-pad pt90">

		<div class="row">
			<div class="col-12 d-flex justify-content-center">
	   			<a href="{{route('servicos')}}" class="link-voltarserv"><i class="fas fa-chevron-left"></i> Voltar para todos os serviços</a>
	   		</div>
		</div>


	</div>

	<!-- Sessão Detalhes do serviço -->
	<div class="container no-pad pt50">

		<div class="row">

			<div class="col-12 col-md-4 col-lg-4">
	   			<img src="{{$servico->image}}" class="img-fluid" style="width: 100%;">
	   		</div>

	   		<div class="col-12 col-md-8 col-lg-8">

	   			<h3 class="playfair-h3-b">{{$servico->title}}</h3>
	   			{!!$servico->content!!}

	   			<div class="box-profi pb30">

	   				<!-- Div para profissional caso preciso rodar loop para vários profissionais-->
	   				<div class="row">
   						<span><h6 class="playfair-h3-g-n">Nº Profissionais no atendimento</h6>{{$servico->numero_profissional}}</span>
	   				</div>
	   				
	   			</div>

	   			<h6 class="playfair-h3-g-n">Faça o agendamento no calendário abaixo!</h6>
	   			<?php  
					$servico->price= number_format($servico->price, 2, ',', '.');
				?>
	   			<div class="preco pb20">R$ {{$servico->price}}</div>

	   		</div>

		</div>

	</div>

	<!-- Sessão Detalhes do serviço -->
	<div class="container no-pad pt50 div-botao">
		<div class="row">
			<div class="col-12 col-md-6 col-lg-6">
				<i class="fas fa-calendar-alt"></i> Selecione a data desejada para agendamento no calendário
				<div id="datepicker"></div>
			</div>

			@if(Auth::guard('loja')->check()) 

				<div class="col-12 col-md-6 col-lg-6 painel-horario">
					<div class="aviso-click"><i class="fas fa-arrow-left"></i> Selecione uma data!</div>
					<i class="far fa-clock"></i> Selecione o melhor horário para seu agendamento
					<?php 

					$dia_da_semana = date('w');

					foreach($agendamento as $agenda => $value){ 
							$dia_sigla= explode('-', $agenda);

					?>

						<!-- 
							A div box-horario é gerada com o conjunto de horario dos dias em que o serviço esta disponivel, deixando o box do dia da semana atual selecionado 
						-->
						<div class="box-horario box-<?php echo $agenda; ?>" <?php if( $dias_numero[$dia_sigla[0]] != $dia_da_semana){ echo 'style="display:none;"'; } ?>>

							
							<div class="row">
								<?php 

									//Monta array com o tempo de intervalo para comparar com o banco
									$tempo['30']= array('900','930','1000','1030','1100','1130','1200','1230','1300','1330','1400','1430','1500','1530','1600','1630','1700','1730','1800','1830','1900','1930','2000','2030');
									$tempo['60']= array('900','1000','1100','1200','1300','1400','1500','1600','1700','1800','1900','2000');
									$tempo['90']= array('900','1030','1200','1330','1500','1630','1800','1930');
									$tempo['120']= array('900','1100','1300','1500','1700','1900');
									$tempo['150']= array('900','1130','1400','1630','1900');

									switch ($servico->tempo_atendimento) {
										case 30:
											$hora_loop= '09:30';
											break;

										case 60:
											$hora_loop= '10:00';
											break;

										case 90:
											$hora_loop= '10:30';
											break;

										case 120:
											$hora_loop= '11:00';
											break;
										
										default:
											$hora_loop= '11:30';
											break;
									}
									
									//hora inicial de atendimento
									$horaInicial = new DateTime('09:00');
									$data_adicional = new DateTime($hora_loop);


									//hora final de atendimento
									$horaFinal = new DateTime(end($tempo[$servico->tempo_atendimento]));

									$data_intervalo= 'PT'.$servico->tempo_atendimento.'M';

									if(in_array($agenda.'-'.$tempo[$servico->tempo_atendimento][0], $value)){
										$classe_inicial= 'hora-ativo';
									}else{
										$classe_inicial= 'hora-inativo';
									}


									//echo date('H:i',strtotime('+1 hour +20 minutes',strtotime('14:00:00')));



									echo '

										<div class="col-4 hora">
											<button type="button" id="'.$agenda.'-'.$tempo[$servico->tempo_atendimento][0].'" class="mt10 '.$agenda.'-'.$tempo[$servico->tempo_atendimento][0].' '.$classe_inicial.'">
												<i class="fas fa-circle"></i> <span>'.$horaInicial->format('H:i').' as '.$data_adicional->format('H:i').'</span>
											</button>
										</div>';

									$auxiliar=1;//Começa em 1 pois o primeiro horario ja foi definido antes do while

									//O valor é somado dentro do while, para evitar que repita a hora final
									while($horaInicial->add(new DateInterval($data_intervalo)) < $horaFinal) { 

										if(in_array($agenda.'-'.$tempo[$servico->tempo_atendimento][$auxiliar], $value)){
											$classe= 'hora-ativo';
										}else{
											$classe= 'hora-inativo';
										}

										if($auxiliar == 1){ 
											$data_adicional = new DateTime($hora_loop); 
										}

										$data_adicional= $data_adicional->add(new DateInterval($data_intervalo));

									  echo '

									  	<div class="col-4 hora">
											<button type="button" id="'.$agenda.'-'.$tempo[$servico->tempo_atendimento][$auxiliar].'" class="mt10 '.$agenda.'-'.$tempo[$servico->tempo_atendimento][$auxiliar].' '.$classe.'">
												<i class="fas fa-circle"></i> <span classs="horario_visual">'.$horaInicial->format('H:i').' as '.$data_adicional->format('H:i').'</span>
											</button>
										</div>';


									$auxiliar++;
									}


									// if(in_array($agenda.'-'.end($tempo[$servico->tempo_atendimento]), $value)){
									// 	$classe_final= 'hora-ativo';
									// 	echo '

									// 	<div class="col-4 hora">
									// 		<button type="button" id="'.$agenda.'-'.end($tempo[$servico->tempo_atendimento]).'" class="mt10 '.$agenda.'-'.end($tempo[$servico->tempo_atendimento]).' '.$classe_final.'">
									// 			<i class="fas fa-circle"></i> <span>'.$horaFinal->format('H:i').'</span>
									// 		</button>
									// 	</div>';

									// }else{
									// 	$classe_final= 'hora-inativo';
									// }

								  ?>
							</div>
							
						</div>
					<?php } ?>

					<form action="{{route('addcarrinho.painel')}}" method="POST">
    					{{ csrf_field() }}
				  
				  		<input type="hidden" name="titulo_servico" value="{{$servico->title}}">
				  		<input type="hidden" name="preco_servico" value="{{$servico->price}}">
				  		<input type="hidden" name="id_servico" value="{{$servico->id}}">
				  		<input type="hidden" name="numero_profissional" value="{{$servico->numero_profissional}}">
    					<input type="hidden" class="data_agendamento" name="data_agendamento" value="<?php if(!in_array(date('j-n-Y'), $data_excecao)){ echo date('j-n-Y'); } ?>">
    					<input type="hidden" class="hora_agendamento" name="hora_agendamento" value="">
    					<input type="hidden" class="id_hora_agendamento" name="id_hora_agendamento" value="">
    					<input type="hidden" name="imagem_servico" value="{{$servico->image}}">
    					<input type="hidden" class="identificador" name="identificador" value="{{$servico->title.'_'.$servico->id.'_'.date('j-n-Y')}}">
						<div class="finalizar_agendamento">
							<div class="pt30"><i class="fas fa-times"></i> Prosseguir com Agendamento </div>
						  	<button class="btn-finalizar-compra btn-fundo-branco">Finalizar Agendamento</button>
						</div>

					</form>
				</div>
 
			@else

				<div class="col-12 col-md-6 col-lg-6">
					<div class="pt50">
						<p class="d-flex justify-content-center">Para agendar um horário, você</p>
						<p class="d-flex justify-content-center">preciso logar em sua conta!</p>
					</div>

					<div class="d-flex justify-content-center pt50">
						<button class="btn-verde centered" data-toggle="modal" data-target="#modal-login" type="button">Entrar ou Cadastrar</button>
					</div>

				</div>

			@endif
		</div>
	</div>

	{{ csrf_field() }}

</div>

@endsection


@section('scripts')

<script type="text/javascript" charset="utf-8" >


	$(document).ready(function() {

		
		// Função de configuração calendario datepicker jquery
		$(function() {

	        $("#datepicker").datepicker({
		        dateFormat: 'dd/mm/yy',
		        dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
		        dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
		        dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
		        monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		        monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
		        nextText: 'Próximo',
		        prevText: 'Anterior',
	            beforeShowDay: enableWednesday,
	            minDate: 0,
	            onSelect: function(dateText, inst) { //Funçao chamada ao selecionar uma data
	                var date = $(this).datepicker('getDate');
	                var dayOfWeek = date.getUTCDay();
	                var dataselecionada= new Date(date);
	                var strDateTime =  dataselecionada.getDate() + "-" + (dataselecionada.getMonth()+1) + "-" + dataselecionada.getFullYear();

	                $('.hora-reservada').removeAttr('disabled');
	                $('button').removeClass('hora-reservada');

	                var data_click_db= dataselecionada.getFullYear() + "-" + (dataselecionada.getMonth()+1) + "-" + dataselecionada.getDate();

	                buscadiasagendado(data_click_db);

	                var identificador_item= $('.identificador').attr('value');

	                var identificador= identificador_item.split('_');

	                $('.identificador').val(identificador[0]+'_'+identificador[1]+'_'+strDateTime);

	                //alert(identificador_item);

	                $('.hora-ativo').removeAttr('disabled');//Ativa os botões de hora
	                $('.aviso-click').hide();//Oculta a div de aviso para clicar em um dia da agenda
	                $('.box-horario').css('opacity','1');

	                $('.data_agendamento').val(strDateTime);

	                 $('.box-horario').hide();
	                 if(dayOfWeek == 0){
	                    $('.box-dom').show();
	                 }else if(dayOfWeek == 1){
	                    $('.box-seg').show();
	                 }else if(dayOfWeek == 2){
	                    $('.box-ter').show();
	                 }else if(dayOfWeek == 3){
	                    $('.box-qua').show();
	                 }else if(dayOfWeek == 4){
	                    $('.box-qui').show();
	                 }else if(dayOfWeek == 5){
	                    $('.box-sex').show();
	                 }else{
	                    $('.box-sab').show();
	                 }

	                $('.hora-ativo').removeClass('hora-selecionada');
	                $('.btn-finalizar-compra').attr('disabled', 'disabled');
	                $('.btn-finalizar-compra').addClass('btn-fundo-branco_disabled');
	                $('.btn-finalizar-compra').removeClass('btn-fundo-branco');
	                $('.finalizar_agendamento .fas').removeClass('fa-check');
	                $('.finalizar_agendamento .fas').addClass('fa-times');

	            }
		    });
	    });

	    //Função que desativa os dias da semana com base nos dias disponiveis do serviço
	    function enableWednesday(date) {
	        
	       var data_calendario= date.getDate()+'-'+(date.getMonth()+1)+'-'+date.getFullYear();

	       
	        if ( jQuery.inArray(date.getDay(), dia_semana_servico ) !== -1 ) {
	             return (jQuery.inArray(data_calendario, dias_excecao) == -1)? [true, ''] : [false, ''] ;
	        }else{
	            return [false, ''];
	        }
	    }

	    var dia_semana_servico= new Array();
	    $.each($( ".diasemana" ), function( index, value ) {
	            dia_semana_servico.push($(this).data('numerodia'));
	    }); 


	    /* Tratamento dos botões de hora da pagina de serviço */
	    $('.btn-finalizar-compra').attr('disabled', 'disabled');
	    $('.btn-finalizar-compra').addClass('btn-fundo-branco_disabled');
	    $('.btn-finalizar-compra').removeClass('btn-fundo-branco');
	    $('.hora-inativo').attr('disabled', 'disabled');

	    $('.hora-ativo').attr('disabled', 'disabled');
	    $('.box-horario').css('opacity','0.2');

	    $('.hora-ativo').on('click', function(){

	        $('.hora-ativo').removeClass('hora-selecionada');

	        var hora_agendamento= $(this).find('span').html();
	        $('.hora_agendamento').val(hora_agendamento);

	        var id_hora= $(this).attr('id');

	        var identificador_item= $('.identificador').attr('value');
	        var identificador= identificador_item.split('_');

	        $('.identificador').val(identificador[0]+'_'+identificador[1]+'_'+identificador[2]+'_'+id_hora);


	        $('.id_hora_agendamento').val(id_hora);
	        $('#'+id_hora).addClass('hora-selecionada');
	        $('.btn-finalizar-compra').removeAttr('disabled');
	        $('.btn-finalizar-compra').removeClass('btn-fundo-branco_disabled');
	        $('.btn-finalizar-compra').addClass('btn-fundo-branco');
	        $('.finalizar_agendamento .fas').removeClass('fa-times');
	        $('.finalizar_agendamento .fas').addClass('fa-check');

	    });


		function buscadiasagendado( data_agenda ){

			
			if(data_agenda == '' || data_agenda == undefined){ 

				var d = new Date();
				var data_agenda = d.getFullYear() + "-" + (d.getMonth()+1) + "-" + d.getDate();

				console.log(data_agenda);

			 }

			 console.log(data_agenda);

			/*
				
			*/
			$.ajax({
				headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          		url : "{{route('checa.agenda')}}",         
          		type : "post",
          		data : {
             		data_agenda : data_agenda,
             		servico_id : "{{$servico->id}}",
          		},
          		beforeSend : function(){
               		$("#resultado").html("ENVIANDO...");
         		}
		    })
		    .done(function(retorno){

		    	var agenda= $.parseJSON(retorno);

		    	console.log(agenda);

		    	$( agenda ).each(function( index ) {
		    		$( "div.box-horario" ).find( "button#"+agenda[index]['id_hora_agendamento'] ).addClass( "hora-reservada" ).attr('disabled', 'disabled');
				});


		    	


		        
		    })
		    .fail(function(jqXHR, textStatus, msg){
		        //alert(msg);
		    }); 

			

		}



	});

</script>

@endsection