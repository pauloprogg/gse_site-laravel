<!-- Modal -->
<div class="modal fade" id="modal-login" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-login modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      <div class="modal-body m-login">
        <div class="row">

          <div class="col-12 col-md-6 col-lg-6 box-login">

            <div class="row pt30">
              <div class="d-none d-sm-block col-md-3 col-lg-3">
                <img class="img-logo" src="{{ asset('assets/site/images/logo.png')}}" alt="" width="90">
              </div>
              <div class="col-12 col-md-7 col-lg-7">
                <h1 class="playfair-h1-g">Entrar</h1>
              </div>
              <div class="col-md-2 col-lg-2"></div>
            </div>

            <form action="{{route('loja.validate')}}" method="POST">
              {{ csrf_field() }}

              <div class="form-group row pt40">

                @if (session('message'))
                  <div class="col-md-12"><div class="alert-{{ session('class') }} alert">{{ session('message') }}</div></div>
                @endif

                <div class="col-12"> <!-- <title> do conteúdo -->
                  <label for="title">Email</label>
                  <input class="form-control" type="email" name="email" id="emaillogin" required/>
                  <div class="email-aviso invalid-feedback">
                    Campo obrigatório.
                  </div>
                </div>
              

                <div class="col-12 pt30 campo-senha"> <!-- <title> do conteúdo -->
                  <label for="title">Senha</label>
                  <input class="form-control senhalogin" type="password" name="password" id="senhalogin" required/>
                  <div class="senha-aviso invalid-feedback">
                    Campo obrigatório.
                  </div>
                  <i class="descobrir-senha far fa-eye"></i>
                </div>

              </div>

              <div class="form-group row form-check">

                <div class="col-12 col-md-7 col-lg-7 pt30">
                  <input class="form-check-input" type="checkbox" name="lembrar" value="1">
                  <label class="form-check-label">
                    Lembrar Senha
                  </label>
                </div>

                <div class="col-12 col-md-5 col-lg-5 pt30">
                  <button class="btn-verde ajaxSubmit" type="submit">Entrar</button>
                </div>

              </div>

            </form>

              <div class="row">
                
                <div class="col-12 d-flex justify-content-center pt30">
                  <a href="" class="link-resetar" alt="Resetar senha">Esqueci minha senha</a>
                </div>

                <span class="linha-login pt40"></span>


              </div>

             <div class="form-group row form-check">

              <div class="col-12 col-md-7 col-lg-7 pt30 d-flex align-items-center pergunta">
                Ainda não tem conta?
              </div>

              <div class="col-12 col-md-5 col-lg-5 pt30">
                <a href="{{route('cadastro')}}" class="btn-branco" type="submit">Cadastre-se</a>
              </div>

            </div>

          </div>

          <div class="d-none d-sm-block col-md-6 col-lg-6 no-pad" style="background-image: url({{ asset('assets/site/images/bglogin.png')}});    background-size:cover;">
          </div>

        </div>
      </div>
    </div>
  </div>
</div>