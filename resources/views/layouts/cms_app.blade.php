<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>{{$title}} - CMS Sorocabacom (Laravel 5.7)</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="Themesbrand" name="author" />
        <link rel="shortcut icon" href="{{ URL::asset('assets/cms/images/favicon.png')}}">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        @yield('css')
        
        <link href="{{ URL::asset('assets/cms/css/metismenu.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset('assets/cms/css/icons.css')}}" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset('assets/cms/css/style.css')}}" rel="stylesheet" type="text/css">
    </head>

    <body>
        <div id="wrapper">
            
            @include('layouts.cms_top')
            @include('layouts.cms_sidebar')

            @yield('content')

            @include('layouts.cms_footer')

            <!-- jQuery  -->
            <script src="{{ URL::asset('assets/cms/js/jquery.min.js')}}"></script>
            <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
            <script src="{{ URL::asset('assets/cms/js/bootstrap.bundle.min.js')}}"></script>
            <script src="{{ URL::asset('assets/cms/js/metisMenu.min.js')}}"></script>
            <script src="{{ URL::asset('assets/cms/js/jquery.slimscroll.js')}}"></script>
            <script src="{{ URL::asset('assets/cms/js/waves.min.js')}}"></script>
            <script src="{{ URL::asset('assets/cms/js/jquery.mask.min.js')}}"></script>
            <script src="{{ URL::asset('assets/cms/js/dobpicker.js')}}"></script>


            
            

            <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script> -->

            @yield('scripts')
            
            <!-- App js -->
            <script src="{{ URL::asset('assets/cms/js/app.js')}}"></script>

        </div>
</body>
</html>