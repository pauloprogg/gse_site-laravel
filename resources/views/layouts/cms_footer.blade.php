<!-- Footer -->
<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                © {{date('Y')}} Sorocabacom <span class="d-none d-sm-inline-block"> - Crafted with [LARAVEL 5.7]</span>.
            </div>
        </div>
    </div>
</footer>
<!-- End Footer -->