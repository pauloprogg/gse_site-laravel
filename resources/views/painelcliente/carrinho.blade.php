<?php
    
    $mes['01']= 'Jan';
    $mes['02']= 'Fev';
    $mes['03']= 'Mar';
    $mes['04']= 'Abr';
    $mes['05']= 'Mai';
    $mes['06']= 'Jun';
    $mes['07']= 'Jul';
    $mes['08']= 'Ago';
    $mes['09']= 'Set';
    $mes['10']= 'Out';
    $mes['11']= 'Nov';
    $mes['12']= 'Dez';


?>

@extends('layouts.app')

@section('content')

<div class="container-fluid pt90 carrinho">

	<!-- Cabeçalho pagina -->
    <div class="row pt50">
        <div class="col-12">
            <h1 class="playfair-h1-g">Carrinho de compra</h1>
        </div>
    </div>

    @if($msg_finalizacao_compra != '')
        <div class="alert alert-warning pt20" role="alert">
            {{$msg_finalizacao_compra}}
        </div>
    @endif

    <div class="row">
        <div class="col-12">
            <div class="alert-success alert" style="display: none;">Serviço excluido com sucesso!</div>
        </div>
        <div class="col-12">
            <div class="alert alert-warning" role="alert" style="display: none;">
              Opss! Ocorreu algum erro ao excluir o item. Tente mais tarde ou entre em contato com o adminstrador do Website.
            </div>
        </div>
    </div>

    <div class="row pt30">
        <div class="col-4">
            
        </div>
        @if(!empty($carrinho))
            <div class="col-3 d-flex justify-content-center">
                <h5 class="playfair-h5-g" style="font-style: normal;">Data</h5>
            </div>
            <div class="col-3 d-flex justify-content-center">
                <h5 class="playfair-h5-g" style="font-style: normal;">Preço</h5>
            </div>
        @endif
        <div class="col-2">
            
        </div>
    </div>
    @php 
    $aux=0;
    @endphp
    @if(!empty($carrinho))
    @foreach($carrinho as $produto)

        <div id="carrinhoitem_{{$aux}}" class="row pt20">
            <div class="col-12 col-md-4 col-carrinho-1">
                <div class="row pt20 pb20">
                    <div class="col-5 img-carrinho">
                        <img class="img-fluid" src="{{$produto['imagem_servico']}}">
                    </div>
                    <div class="col-7">
                        <p style="font-weight: bold;">{{$produto['titulo_servico']}}</p>
                        <p>
                            <span class="playfair-h5-g">Nº de Profissionais</span><br>
                            <span>{{$produto['numero_profissional']}}</span>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-3 col-carrinho-2 pt40">
                <div class="row">
                    <div class="col-12 d-flex justify-content-center">
                        <span style="font-weight: bold;"><?php $mes_ano= date('m', strtotime($produto['data_agendamento'])); echo date('d', strtotime($produto['data_agendamento'])).' '.$mes[$mes_ano]; ?></span>
                    </div>
                    <div class="col-12 d-flex justify-content-center">
                        <span><?php echo str_replace('as', ' - ', $produto['hora_agendamento']); ?></span>
                    </div>
                </div>
                
            </div>
            <div class="col-12 col-md-3 col-carrinho-3 d-flex justify-content-center pt60">
                <h3 class="valor_servico" data-valor="<?php echo str_replace(",",".",$produto['preco_servico']); ?>" style="font-weight: bold;">R$ {{$produto['preco_servico']}}</h3>
            </div>             
            <div class="col-12 col-md-2 col-carrinho-4 d-flex justify-content-center pt60">
                    <a href="{{route('pedido.delete', $produto['identificador'])}}" class="delete-item"><i class="fas fa-trash-alt"></i></a>
                
            </div>
        </div>
    @php $aux++; @endphp
    @endforeach
    @endif

    @if(!empty($carrinho))
        <div class="row pt20 pb20">
            <div class="col-1">
                <a class="agendar-ervico d-flex justify-content-center" href="{{route('servicos')}}"><i class="fas fa-plus"></i></a>
            </div>
            <div class="col-11 pt20">
                Agendar outro serviço
            </div>
        </div>
    @else
        <div class="row pt20 pb20">
            <div class="col-12 d-flex justify-content-center">
                Você não possui itens no carrinho...
            </div>
            <div class="col-12 d-flex justify-content-center">
                <a class="agendar-ervico d-flex justify-content-center" href="{{route('servicos')}}"><i class="fas fa-plus"></i></a>
            </div>
        </div>
    @endif

    @if(!empty($carrinho))
    <div class="row linha-total pt15 pb10">
        <dvi class="col-4"></dvi>
        <dvi class="col-4 pt5"><h5 class="playfair-h5-g" style="font-style: normal;">Total</h5></dvi>
        <dvi class="col-4"><h3 class="valor_total" style="font-weight: bold;"></h3></dvi>
    </div>

     <div class="row linha-total pt15 pb10">
        <dvi class="col-6"></dvi>
        <dvi class="col-6 d-flex justify-content-right">
            <a href="{{route('carrinho.limpar')}}" class="btn-branco" style="background:none !important; margin-top: 0px;">Limpar Carrinho</a>
            <a href="{{route('finalizar.compra')}}" class="btn-verde-carrinho" style=" margin-top: 0px;">Finalizar Compra</a>
        </dvi>
    </div>
    @endif

    {{ csrf_field() }}

</div>

@endsection


@section('scripts')


<script type="text/javascript" charset="utf-8" >
        
        $(document).ready(function() {

            somaValorservico();

            function somaValorservico() { 

                valor_total= 0;
                $.each($( ".valor_servico" ), function( index, value ) {
                    var valor= $(this).data('valor');
                    var valor_number= new Number(valor);

                    valor_total += valor_number;

                });

                valor_total= Number(valor_total).toFixed(2);

                mascaraValor(valor_total);

            }

            function alteraValorservico(valor, id_div_delete) { 


                var valor_number= new Number(valor);

                valor_total -= valor_number;

                valor_total= Number(valor_total).toFixed(2);

                mascaraValor(valor_total);

                $( "#"+id_div_delete ).remove();                

            }

            function mascaraValor(valor) {
                valor = valor.toString().replace(/\D/g,"");
                valor = valor.toString().replace(/(\d)(\d{8})$/,"$1.$2");
                valor = valor.toString().replace(/(\d)(\d{5})$/,"$1.$2");
                valor = valor.toString().replace(/(\d)(\d{2})$/,"$1,$2");

                $('.valor_total').html('').append('R$ '+valor);                    
            }

        	
            // $('.delete-item').on('click', function(){
            //     var identificador= $(this).attr('id');
            //     var valor= $(this).data('valordelete');
            //     var id_div_delete= $(this).data('iddiv');

            //     var CSRF_TOKEN = $('input[name="_token').val();



            //     $.ajax({
            //      type: 'POST',
            //      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            //      url: "{{$urlajax_delete}}",
            //      identificador: identificador,
            //      success: function() {
            //         $('.alert-success').show();
            //         setTimeout(function(){ 
            //             $('.alert-success').hide('slow'); 
            //         }, 5000);

            //         alteraValorservico(valor, id_div_delete);

            //         var quant_itens = $( ".valor_servico" ).length;
            //     }
                 
            //    });



               /* $.post("{{$urlajax_delete}}", { 'X-CSRF-TOKEN': CSRF_TOKEN, identificador:identificador }, 
                    function(data){

                        console.log(data);

                        $('.alert-success').show();
                         
                        setTimeout(function(){ 
                            $('.alert-success').hide('slow'); 
                        }, 5000);

                        alteraValorservico(valor, id_div_delete);

                        var quant_itens = $( ".valor_servico" ).length;

                        if(quant_itens == 0){
                            //window.location.replace("{{route('carrinho.painel')}}");
                        }else{
                            $('.icon-quantidade').html('').append(quant_itens);
                        }

                }).fail(function(){
                      
                      $('.alert-warning').show();
                      setTimeout(function(){ 
                        $('.alert-warning').hide('slow'); 
                    }, 5000);
                });*/
            

            // });



        });

</script>

@endsection