@extends('layouts.app')

@section('content')

<!-- Sessão banner topo -->
<div class="container">
	<div class="row pt90">
	 	<div class="col-12">
	        <h1 class="playfair-h1-g">Faça seu login!</h1>
	    </div>
    </div>

    @if($msg_cadastro != '')
        <div class="alert alert-success pt20" role="alert">
            {{$msg_cadastro}}
        </div>
    @endif

    <div class="row">
    	
    	<div class="col-md-3 col-lg-3"></div>
    	<div class="col-12 col-md-6 col-lg-6">
    		<div class="box-login">
    			@if (session('message'))
					<div class="col-md-12"><div class="alert-{{ session('class') }} alert">{{ session('message') }}</div></div>
				@endif
    			<form action="{{route('loja.validate')}}" method="post" accept-charset="utf-8">
    				{{ csrf_field() }}

               		<div class="form-group row pt40">

			            <div class="col-12"> <!-- <title> do conteúdo -->
			              <label for="title">Email</label>
			              <input class="form-control" type="email" name="email" id="emaillogin" required/>
			              <div class="email-aviso invalid-feedback">
			                Campo obrigatório.
			              </div>
			            </div>
              

		                <div class="col-12 pt30 campo-senha"> <!-- <title> do conteúdo -->
		                  <label for="title">Senha</label>
		                  <input class="form-control senhalogin" type="password" name="password" id="senhalogin" required/>
		                  <div class="senha-aviso invalid-feedback">
		                    Campo obrigatório.
		                  </div>
		                  <i class="descobrir-senha far fa-eye"></i>
		                </div>

              		</div>

              		<div class="form-group row form-check">

		                <div class="col-12 col-md-7 col-lg-7 pt30">
		                  <input class="form-check-input" type="checkbox" name="lembrar" value="1">
		                  <label class="form-check-label">
		                    Lembrar Senha
		                  </label>
		                </div>

		                <div class="col-12 col-md-5 col-lg-5 pt30">
		                  <button class="btn-verde" type="submit">Entrar</button>
		                </div>

		            </div>


    			</form>	
    		</div>
    	</div>
    	<div class="col-md-3 col-lg-3"></div>
    </div>
</div>

@endsection