@extends('layouts.app')

@section('content')

<div class="container-fluid pt90">

	<!-- Cabeçalho pagina -->
    <div class="row pt50">
        <div class="col-12">
            <h1 class="playfair-h1-g">Painel do Cliente</h1>
        </div>
    </div>

	<div class="row">
		<div class="col-12 col-md-3">
			<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
			  <a class="nav-link active" id="v-pills-pedidos-tab" data-toggle="pill" href="#v-pills-pedidos" role="tab" aria-controls="v-pills-pedidos" aria-selected="true">Pedidos</a>
			  <a class="nav-link" id="v-pills-dados-tab" data-toggle="pill" href="#v-pills-dados" role="tab" aria-controls="v-pills-dados" aria-selected="false">Dados de Contato</a>
			  <a class="nav-link" id="v-pills-perfil-tab" data-toggle="pill" href="#v-pills-perfil" role="tab" aria-controls="v-pills-perfil" aria-selected="false">Perfil</a>
			  <a class="nav-link" style="padding-top: 40px;" href="{{route('loja.logout')}}">Sair</a>
			</div>
		</div>
		<div class="col-12 col-md-9">
			 <!-- Verifica e mostra mensagem de sucesso -->
                @include('cms.includes.alert_messages')
			<form action="{{route('userpainel.update')}}" method="POST" enctype="multipart/form-data">
				{{ csrf_field() }}

				<input type="hidden" name="user_id" value="{{$auth->id}}">

				<div class="tab-content" id="v-pills-tabContent">

				  <div class="tab-pane fade show active" id="v-pills-pedidos" role="tabpanel" aria-labelledby="v-pills-pedidos-tab">
				  	<div class="card">
					  <h5 class="card-header">Pedidos ({{count($pedidos)}})</h5>
					  <div class="card-body">


					  	<table class="table table-hover">
						  <thead>
						    
						    @if(count($pedidos) > 0)

						    	<tr>
							      <th scope="col"># PEDIDO</th>
							      <th scope="col">TITULO</th>
							      <th scope="col">DATA</th>
							      <th scope="col">VALOR</th>
							      <th scope="col">STATUS</th>
							    </tr>

						    	@foreach($pedidos as $pedido)
						    		@php $valor= number_format($pedido->price, 2, ',', '.') @endphp
						    		<tr>
						    			<td scope="row">{{$pedido->id}}</td>
								    	<td>{{$pedido->titulo_produto}}</td>
								    	<td>{{date("d/m/Y", strtotime($pedido->data_agendamento))}} - {{$pedido->hora_agendamento}}</td>
								    	<td>R$ {{$valor}}</td>
									    <td>
									    	@switch($pedido->statuspg)
											    @case(0)
											        <span class="text-secondary">Em processo</span>
											        @break

											    @case(1)
											        <span class="text-success">Pago</span>
											        @break

											    @default
											        <span class="text-danger">Cancelado</span>
											@endswitch
									    </td>
									    
		                            </tr>
						    	@endforeach

						    @else

						    	<p>Você não possui pedidos...</p>

						    @endif
					    	</thead>
						 
						  </tbody>
						</table>
					    	
					  </div>
					</div>
				  </div>

				  <div class="tab-pane fade" id="v-pills-dados" role="tabpanel" aria-labelledby="v-pills-dados-tab">
				  	<div class="card">
					  <h5 class="card-header">Alterar dados de contato</h5>
					  <div class="card-body">
					    
					  	<div class="row justify-content-center">
	                        <div class="col-12 col-md-6">
		                        <div class="form-group">
		                            <label for="exampleInputEmail1">E-mail</label>
		                            <input type="email" class="form-control email" id="email" name="email" value="{{$auth->email}}" required placeholder="Insira o E-mail">
		                            <div class="aviso-email invalid-feedback">
					                   Por favor preencha o campo e-mail!
					                </div>
		                        </div>
		                        
		                    </div>
	                    </div>

	                    <div class="row justify-content-center">
	                        <div class="col-12 col-md-6">
		                        <div class="form-group">
		                            <label for="exampleInputEmail1">Telefone <small class="text-muted">(Opcional)</small></label>
                            		<input type="text" class="form-control telefone-mask" id="telefone" value="{{$auth->telefone}}" name="telefone" placeholder="Telefone Fixo">
		                        </div>
		                    </div>
	                    </div>

	                    <div class="row justify-content-center">
	                        <div class="col-12 col-md-6">
		                        <div class="form-group">
		                            <label for="exampleInputEmail1">Celular</label>
                            		<input type="text" class="form-control celular-mask" id="celular" value="{{$auth->celular}}" name="celular" value="{{$auth->celular}}" required placeholder="Insira o E-mail">
		                        </div>
		                    </div>
	                    </div>

	                    <div class="row justify-content-center">
	                        <div class="col-12 col-md-6">
	                        	<button type="submit" class="btn btn-success btn-salvar">Salvar Alterações</button>
	                        </div>
	                    </div>
					  </div>
					</div>

				  </div>


				  <div class="tab-pane fade" id="v-pills-perfil" role="tabpanel" aria-labelledby="v-pills-perfil-tab">
				  	<div class="card">
					  <h5 class="card-header">Alterar dados de perfil</h5>
					  <div class="card-body">


					    
					  	<div class="row justify-content-center">
	                        <div class="col-12 col-md-6">
		                        <div class="form-group">
		                            <label for="exampleInputEmail1">Nome</label><br>
                            		{{$auth->name}}
		                        </div>
		                    </div>
	                    </div>

	                    <div class="row justify-content-center">
                            <div class="col-12 col-md-6"> <!-- <image> principal do post -->
                            	<div class="form-group">
                            		<img src="{{$auth->image ? $auth->image : 'https://via.placeholder.com/600x600'}}" alt="" class="mb-3 img-avatar" width="150" /><br>
	                                <small class="observacao badge badge-warning"> Obs: A imagem acima é somente um preview, clique em salvar alterações para salvar a imagem! </small><br><br>
	                                <label for="image">Imagem destaque recomendação de até 300x300 <small class="badge badge-primary">Cabeçalho</small></label>
	                                <input type="file" name="image" class="form-control-file image" />
                            	</div>
                            </div>
                        </div>

	                    <div class="row justify-content-center">
	                        <div class="col-12 col-md-6">
		                        <div class="form-group">
		                            <label for="password">Senha</label>
                            		<input class="form-control senhas senha1" type="password" name="password" />
                            		<div class="aviso-senha-quant invalid-feedback">
					                   Senha deve contem no minimo 6 caracteres!
					                </div>
		                        </div>
		                    </div>
	                    </div>

	                    <div class="row justify-content-center">
	                        <div class="col-12 col-md-6">
		                        <div class="form-group">
		                            <label for="password_confirmation">Confirmar senha</label>
                            		<input class="form-control senhas senha2" type="password" name="password_confirmation" />
                            		<div class="aviso-senha invalid-feedback">
					                   Senhas não conferem!
					                </div>
		                        </div>
		                    </div>
	                    </div>
	                    <div class="row justify-content-center">
	                        <div class="col-12 col-md-6">
	                        	<button type="submit" class="btn btn-success btn-salvar">Salvar Alterações</button>
	                        </div>
	                    </div>


					  </div>
					</div>
				  </div>

				</div>
			</form>
		</div>
	</div>
</div>

@endsection

@section('scripts')

<script type="text/javascript" charset="utf-8" >
        
        $(document).ready(function() {

        	//Função que exibe a imagem antes de subir o arquivo
        	$('.badge-warning').hide();
             $(function () {
                $(".image").change(function () {
                    readURL(this);
                });
            });
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        //alert(e.target.result);
                        $('.img-avatar').attr('src', e.target.result);
                        $('.badge-warning').show();
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }
            //Função que exibe a imagem antes de subir o arquivo

            //Valida o campo de e-mail
            $( ".email" ).keyup(function() {

            	var contagem= $(this).val().length;
 
            	if( contagem == 0 ){
            		$('.btn-salvar').attr('disabled', 'disabled');
            		$('.aviso-email').show();
            		$('.email').addClass('is-invalid');
            	}else{
            		$('.btn-salvar').removeAttr('disabled');
            		$('.aviso-email').hide();
            	}
            	
            });

            // Valida os campos de senha
            $( ".senhas" ).keyup(function() {
			  var valor_senha1= $('.senha1').val();
			  var valor_senha2= $('.senha2').val();

			  if( valor_senha1.length < 6 || valor_senha2.length < 6 ){
			  		$('.aviso-senha-quant .invalid-feedback').show();
			  }else{
			  		$('.aviso-senha-quant .invalid-feedback').hide();
			  }

			  if( valor_senha1 != valor_senha2){
			  	$('.aviso-senha .invalid-feedback').show();
			  	$('.senhas').addClass('is-invalid');
			  	$('.btn-salvar').attr('disabled', 'disabled');

			  }else{
			  	$('.aviso-senha .invalid-feedback').hide();
			  	$('.senhas').removeClass('is-invalid');
			  	$('.btn-salvar').removeAttr('disabled');
			  }

			});

        });

</script>

@endsection