@extends('layouts.app')

@section('content')

<!-- Sessão banner topo -->
<div class="container-fluid no-pad">

    <!-- Session Banner hide only xs-->
    <div class="row pl90 d-none d-sm-block pb50">
        <div class="col-12 banner-conheca" style="background-image: url({{ asset($page->headimg)}});">
            <div class="chamada playfair-h1-w">
                {{$page->headtitle}}
            </div>
            <div class="fiquepordentro">
                Fique por dentro <span class="linha"></span> <i class="fab fa-facebook-f"></i> <i class="fab fa-instagram"></i>
            </div>
        </div>
    </div>

    <!-- Session Banner visible only xs-->
    <div class="row d-block d-sm-none pb90">
        <div class="col-12 banner-home" style="background-image: url({{ asset($page->headimg)}});">
            <div class="chamada playfair-h1-w">
                {{$page->headtitle}}
            </div>
            <div class="fiquepordentro pl20">
                Fique por dentro <span class="linha"></span> <i class="fab fa-facebook-f"></i> <i class="fab fa-instagram"></i>
            </div>
        </div>
    </div>

</div>

<!-- Sessão lista de serviço -->
<div class="container">
	
	@foreach($categorias as $categoria)

	


		@if( count($tiposervico[$categoria->id]) )

			<div class="row">

				<div class="col-12 col-md-8 col-lg-8">

					<h3 id="{{$categoria->slug}}" class="playfair-h3-g">{{$categoria->label}}</h3>
					<p>{{$categoria->content}}</p>

				</div>

			</div>

			<div class="row lista-servico pb90">

				@foreach($tiposervico[$categoria->id] as $tipo)

					<div class="col-12 col-md-4 col-lg-3">
						<div class="box-img"><a href="{{route('servico',$tipo->slug)}}"><img class="img-fluid pb20" src="{{$tipo->image}}" alt="{{$tipo->title}}"></a></div>
						<div class="pb20 titulo-serv">{{$tipo->title}}</div>
						<?php  
							$tipo->price= number_format($tipo->price, 2, ',', '.');
						?>
						<div class="preco pb20">R$ {{$tipo->price}}</div>
					</div>

				@endforeach

			</div>
		
		@endif
		

	@endforeach


</div>
<div class="container-fluid no-pad">
    <!-- Session Conheça -->
    <section class="session-conheca">
        <div class="row pt90 pb100">
            <div class="col-12 pb65">
                <h1 class="playfair-h1-g">Conheça</h1>
            </div>
            <div class="col-md-6 col-lg-5 d-none d-md-block pl90">
                <img class="img-fluid" src="{{$conheca->destaquehomeimg}}">
            </div>
            <div class="col-12 col-md-6 col-lg-6 pb40">
                <h2 class="playfair-h2-b title-conheca">{{$conheca->summary}}</h2>
                <div class="row pt30 subtitle-conheca">
                    <div class="col-3 col-md-4">
                        <span class="linha-conheca"></span>
                    </div>
                    <div class="col-9 col-md-8 sumario">
                        {!!$conheca->content!!}
                        <a href="{{route('conheca')}}" class="btn-branco mr50 d-none d-md-block">Conheça Mais</a>
                    </div>
                </div>
                <div class="col-12 d-sm-block d-md-none">
                    <a href="{{route('conheca')}}" class="btn-branco">Conheça Mais</a>
                </div>
            </div>
            
            <div class="col-12 d-sm-block d-md-none">
                <img class="img-fluid" src="{{$conheca->destaquehomeimg}}">
            </div>
        </div>
    </section>
</div>

@endsection