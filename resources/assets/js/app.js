$( document ).ready(function() {

     /* JQUERY MASK */
    $('.telefone-mask').mask('(00) 0000-0000');
    $('.celular-mask').mask('(00) 00000-0009');


    $('.fa-eye').on('click', function(){
    	$('.descobrir-senha').toggleClass('fa-eye fa-eye-slash');
    	if( $('.descobrir-senha').hasClass('fa-eye') ){
    		$(".senhalogin").prop('type','password');
    	}
    	if( $('.descobrir-senha').hasClass('fa-eye-slash') ){
    		$(".senhalogin").prop('type','text');
    	}
    });

    $.dobPicker({
        daySelector: '#dia', /* Required */
        monthSelector: '#mes', /* Required */
        yearSelector: '#ano', /* Required */
        dayDefault: 'Dia', /* Optional */
        monthDefault: 'Mês', /* Optional */
        yearDefault: 'Ano', /* Optional */
        minimumAge: 18, /* Optional */
        maximumAge: 90 /* Optional */
    });

    var mesdoano = ["Mês","Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];

    $( "#mes option" ).each(function( index ) {
      $( this ).text(mesdoano[index]);
    });

    $('#dia').on('change', function() {
        dia= this.value;
        $('.datanascimento').val('');
    });

    $('#mes').on('change', function() {
        mes= this.value;
        $('.datanascimento').val('');
    });

    $('#ano').on('change', function() {
        ano= this.value;
        datanascimento= ano+'-'+mes+'-'+dia;
        $('.datanascimento').val(datanascimento); 
    });


    $('.ajaxSubmit').on('click', function(){
        var email = $("[name='email']").val();
        var senha = $("[name='password']").val();
        var token = $("[name='_token']").val();

        //Verifica se o campo é vazio e aplica classe de alerta no input e div
        if(email == '' || email == undefined ){
            $("[name='email']").addClass('is-invalid');
            $('.email-aviso .invalid-feedback').css('display','block');
        }

        //Ao digitar no campo valiade retira classe de aviso
        $( "#emaillogin" ).on('click', function() {
            $("[name='email']").removeClass('is-invalid');
            $('.email-aviso .invalid-feedback').css('display','none');
        });

        //Verifica se o campo é vazio e aplica classe de alerta no input e div
        if(senha == '' || senha == undefined ){
            $("[name='password']").addClass('is-invalid');
            $('.descobrir-senha').addClass('descobrir-senha-aviso');
            $('.senha-aviso .invalid-feedback').css('display','block');
        }
        
        //Ao digitar no campo valiade retira classe de aviso
        $( "#senhalogin" ).on('click', function() {
            $("[name='password']").removeClass('is-invalid');
            $('.descobrir-senha').removeClass('descobrir-senha-aviso');
            $('.senha-aviso .invalid-feedback').css('display','none');
        });
       
        //Monta variavel se caso o checkbox for selecionado
        if ($("[name='lembrar']").is(':checked')) {
            var lembrar = $("[name='lembrar']").val();
        }
    });

    $('.close').on('click', function(){
        $("[name='email']").val('').removeClass('is-invalid');
        $("[name='password']").val('').removeClass('is-invalid');
        $('.email-aviso .invalid-feedback').css('display','none');
        $('.senha-aviso .invalid-feedback').css('display','none'); 
    });

    if( $('.erro-login').data('login') == 'erro' ){
        $('.link_login').trigger('click');
    }


});