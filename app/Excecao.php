<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Excecao extends Model
{
    
	protected $table= 'excecoes';

	/* Array de campos protegidos a serem gravados no banco. Ao usar [body] ele aceitará TODOS os campos. */
    protected $fillable = [
        'post_id', 'content', 'data'
    ];


     /* Atribuir POST a uma CATEGORIA. (Posts são pertencentes a uma categoria - belongsTo) */
    public function servico(){
        return $this->belongsTo('App\Post','post_id');
    }


}
