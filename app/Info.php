<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Info extends Model
{
    protected $fillable = [
    	'scriptshead',
        'scriptsfoot',
        'googlemaps',
        'facebook',
        'instagram',
        'twitter',
        'youtube',
        'copyright',
        'footer',
        'telefone1',
        'telefone2',
        'email',
        'endereco',
    ];
}
