<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
	/* Array de campos protegidos a serem gravados no banco. Ao usar [body] ele aceitará TODOS os campos. */
    protected $fillable = [
    	'name', 'title', 'content', 'image', 'galeria_id', 'summary', 'headimg', 'destaquehomeimg', 'destaquehomeserv1', 'destaquehomeserv2', 'destaquehomeserv3', 'imgfix', 'keywords', 'description', 'headtitle'
    ];

    /* Função para adicionar a URL do site automaticamente na imagem após puxar do banco
    URL determinada no .env */
    public function getHeadimgAttribute($value) {
        if($value) {
            return config('app.url').'uploads/'.$value;
        }
    }

    public function getDestaquehomeimgAttribute($value) {
        if($value) {
            return config('app.url').'uploads/'.$value;
        }
    }

    public function getdestaquehomeserv1Attribute($value) {
        if($value) {
            return config('app.url').'uploads/'.$value;
        }
    }

    public function getdestaquehomeserv2Attribute($value) {
        if($value) {
            return config('app.url').'uploads/'.$value;
        }
    }

    public function getdestaquehomeserv3Attribute($value) {
        if($value) {
            return config('app.url').'uploads/'.$value;
        }
    }

    public function getImgfixAttribute($value) {
        if($value) {
            return config('app.url').'uploads/'.$value;
        }
    }

    /* Retorna as exceções relacionadas a um POST */
    public function excecoes(){
        return $this->hasMany('App\Excecao', 'post_id');
    }
}
