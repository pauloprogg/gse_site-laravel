<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;

use View;
use Auth;
use App\Info;
use App\Categoria;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /* Traduz linguagem do DATE em todas as seções do site - Funciona apenas para a função diffForHuman() */
        Carbon::setLocale('pt');  

        View::composer('*', function($view){
            if(Auth::guard('loja')->check()){
                $view->with('auth', Auth::guard('loja')->user());
            }else{
                $view->with('auth', Auth::user());
            }
                
            $view->with('info', Info::findOrFail(1));
            $view->with('categorias', Categoria::all());
        });

        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
