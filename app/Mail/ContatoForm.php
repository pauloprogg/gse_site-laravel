<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContatoForm extends Mailable
{
    use Queueable, SerializesModels;

   protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }



    //---------------------------------------------------------------------------------------
    


    public function build()
    {
        return $this->subject('Contato enviado através do site Go Spa Express')->markdown('emails.contato')->with(['data' => $this->data]);
    }
}
