<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AvisocadastroForm extends Mailable
{
    use Queueable, SerializesModels;

   protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }



    //---------------------------------------------------------------------------------------
    


    public function build()
    {
        return $this->subject('Novo cadastro realizado no site Go Spa Express')->markdown('emails.avisocadastro')->with(['data' => $this->data]);
    }
}
