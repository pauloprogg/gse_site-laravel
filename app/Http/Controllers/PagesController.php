<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PagesRequest;
use App\Http\Requests\ContatoFormRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContatoForm;

use App\Page;
use App\Galeria;
use App\Categoria;
use App\Post;
use App\File;
use App\Info;

class PagesController extends Controller
{
    public function index(Request $request)
    {
        /* 1 = Administrador | 2 = Cliente | 3 = Conteudoria | 4 = Editor | 5 = Redator */
        if(!$request->user()->authorizeRoles(['1','2','3','4'])) {
            $title = "Acesso não autorizado";
            return view('cms.errors.401', compact('title'));
        }
        
        $pages = Page::all();
        $title = 'Listagem de Páginas';

        return view('cms.pages.index', compact('title', 'pages'));
    }

    public function edit(Request $request, $id)
    {
        /* 1 = Administrador | 2 = Cliente | 3 = Conteudoria | 4 = Editor | 5 = Redator */
        if(!$request->user()->authorizeRoles(['1','2','3','4'])) {
            $title = "Acesso não autorizado";
            return view('cms.errors.401', compact('title'));
        }
        
        $galeria = Galeria::all();
        $page = Page::findOrFail($id);
        $title = 'Editando: '.$page->name;
        return view('cms.pages.edit', compact('title', 'page', 'galeria'));
    }

    public function update(PagesRequest $request, $id)
    {
        $page = Page::findOrFail($id);
        $up = $request->all();
        
        if($request->hasFile('headimg')){
            $file = $request->file('headimg');
            $file_name = time().'-'.$file->getClientOriginalName();
            $file_path = 'uploads/';

            $file->move($file_path, $file_name);

            $up['headimg'] = $file_name;

        }

        if($request->hasFile('destaquehomeimg')){
            $file = $request->file('destaquehomeimg');
            $file_name = time().'-'.$file->getClientOriginalName();
            $file_path = 'uploads/';

            $file->move($file_path, $file_name);

            $up['destaquehomeimg'] = $file_name;

        }

        // Roda um loop para os campos de destaques da home em serviços
        $aux=1;
        while ($aux <= 3) {
            $arquivo= 'destaquehomeserv'.$aux;
            if($request->hasFile($arquivo)){
                $file = $request->file($arquivo);
                $file_name = time().'-'.$file->getClientOriginalName();
                $file_path = 'uploads/';

                $file->move($file_path, $file_name);

                $up['destaquehomeserv'.$aux] = $file_name;

            }
            $aux++;
        }

        if($request->hasFile('imgfix')){
            $file = $request->file('imgfix');
            $file_name = time().'-'.$file->getClientOriginalName();
            $file_path = 'uploads/';

            $file->move($file_path, $file_name);

            $up['imgfix'] = $file_name;
            
        }
        

        $page->update($up);

        Session::flash('message', 'Editado com sucesso!');
        Session::flash('class', 'success');
        return redirect()->route('pages.edit', $id);
    }


    public function home()
    {

        $page = Page::findOrFail(1);
        $conheca = Page::findOrFail(2);
        $servico = Page::findOrFail(3);

        
        return view('home', compact('page', 'conheca', 'servico'));
    }

    public function conheca()
    {

        $page = Page::findOrFail(2);
        $galeria = Galeria::findOrFail($page->galeria_id);
        $servico = Page::findOrFail(3);
        
        return view('conheca', compact('page', 'galeria','servico'));
    }
    

    public function servicos()
    {

        $page = Page::findOrFail(3);
        $servicos= Post::all();
        $categorias= Categoria::all();
        $conheca = Page::findOrFail(2);

        foreach ($categorias as $cat) {
           $tiposervico[$cat->id] = Post::where('categoria_id', $cat->id)->get();
        }
        
        return view('servicos', compact('page','servicos','categorias','tiposervico', 'conheca'));
    }


    public function servico( Request $request, $slug ){

        $page = Page::findOrFail(3);
        $servico= Post::where('slug', $slug)->first();
        $agendamento= json_decode($servico->agendamento, true);

        return view('servico', compact('page','servico', 'agendamento'));

   }


    public function contato()
    {

        $page = Page::findOrFail(7);
        $conheca = Page::findOrFail(2);
        $mensagem_contato= session()->get('msg');//Mensagem recebida após envio do form contato
        
        return view('contato', compact('page', 'mensagem_contato', 'conheca'));
    }


     

    public function contatoenviar( ContatoFormRequest $request ){

        $contato= $request->all();

        $data = [
            'nome' => $contato['name'],
            'email' => $contato['email'],
            'assunto' => $contato['assunto'],
            'mensagem' => $contato['mensagem']
        ];



        $info = Info::findOrFail(1);

        $sendto= $info->email;

        //dd($request->nome);
        Mail::to($sendto)->send( new ContatoForm($data) );
    
        return redirect()->route('contato')->with('msg', 'E-mail enviado com sucesso!! Em breve entraremos em contato.');
        

    }



    public function privacidade()
    {

        $page = Page::findOrFail(9); 
        
        return view('privacidade', compact('page'));
    }

    public function compra()
    {

        $page = Page::findOrFail(5);
        
        return view('compra', compact('page'));
    }

    public function cancelamento()
    {

        $page = Page::findOrFail(6);
        
        return view('cancelamento', compact('page'));
    }



}