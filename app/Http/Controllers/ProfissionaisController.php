<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PostsRequest;
use Illuminate\Support\Facades\Session;

use App\Profissional;
use App\File;

class ProfissionaisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        /* 1 = Administrador | 2 = Cliente | 3 = Conteudoria | 4 = Editor | 5 = Redator */
        if(!$request->user()->authorizeRoles(['1','2','3','4'])) {
            $title = "Acesso não autorizado";
            return view('cms.errors.401', compact('title'));
        }

        $profissionais = Profissional::all();
        $title = 'Listagem de Profissionais';

        return view('cms.profissionais.index', compact('title', 'profissionais'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        /* 1 = Administrador | 2 = Cliente | 3 = Conteudoria | 4 = Editor | 5 = Redator */
        if(!$request->user()->authorizeRoles(['1','2','3','4'])) {
            $title = "Acesso não autorizado";
            return view('cms.errors.401', compact('title'));
        }

        $title = 'Inserindo novo Profissional';

        return view('cms.profissionais.create', compact('title'));
    }


     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        /* 1 = Administrador | 2 = Cliente | 3 = Conteudoria | 4 = Editor | 5 = Redator */
        if(!$request->user()->authorizeRoles(['1','2','3','4'])) {
            $title = "Acesso não autorizado";
            return view('cms.errors.401', compact('title'));
        }

        $profissional = Profissional::findOrFail($id);
        $title = 'Editando: '.$profissional->name;

        if($request->hasFile('perfilimg')){
            $file = $request->file('perfilimg');
            $file_name = time().'-'.$file->getClientOriginalName();
            $file_path = 'uploads/profissionais/';

            $file->move($file_path, $file_name);

            $up['perfilimg'] = $file_name;

        }

        return view('cms.profissionais.edit', compact('title', 'profissional'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $new = $request->all();

        if($request->hasFile('perfilimg')){
            $file = $request->file('perfilimg');
            $file_name = time().'-'.$file->getClientOriginalName();
            $file_path = 'uploads/profissionais/';

            $file->move($file_path, $file_name);

            if($new['perfilimg'] != "") {
                $new['perfilimg'] = $file_name;
            }
        }

        Profissional::create($new);

        Session::flash('message', 'Adicionado com sucesso!');
        Session::flash('class', 'success');
        return redirect()->route('profissionais.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $profissional = Profissional::findOrFail($id);
        $up = $request->all();

        if($request->hasFile('perfilimg')){
            $file = $request->file('perfilimg');
            $file_name = time().'-'.$file->getClientOriginalName();
            $file_path = 'uploads/profissionais/';

            $file->move($file_path, $file_name);

            if($up['perfilimg'] != "") {
                $up['perfilimg'] = $file_name;
            }
        }

        $profissional->update($up);

        Session::flash('message', 'Editado com sucesso!');
        Session::flash('class', 'success');
        return redirect()->route('profissionais.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        /* 1 = Administrador | 2 = Cliente | 3 = Conteudoria | 4 = Editor | 5 = Redator */
        if(!$request->user()->authorizeRoles(['1','2','3','5'])) {
            $title = "Acesso não autorizado";
            return view('cms.errors.401', compact('title'));
        }
        
        $profissional = Profissional::findOrFail($id);

        $profissional->delete();

        Session::flash('message', 'Removido com sucesso!');
        Session::flash('class', 'danger');
        return redirect()->route('profissionais.index');
    }
}
