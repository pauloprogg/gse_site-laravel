<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;
use App\Mail\Avisocompra;

use App\Info;
use App\Pedido;
use App\Userloja;

class RetornopagamentoController extends Controller
{

	public function separaparametros($parametro_url)
	{

		$parametros= explode('?', $parametro_url);
    	$parametros= explode('&', $parametros[1]);
    	$parametro[]= '';

    	foreach ($parametros as $param) {
    		
    		$param= explode('=', $param);
    		$parametro[$param[0]]= $param[1];
	
    	}

    	return $parametro;

	}


     public function pagamentostatus(Request $request)
    {

    	$url= url()->full();

    	$parametro= self::separaparametros($url);



    	switch ($parametro['collection_status']) {
    		case 'in_process':
    			 $status_pg= 0;
    			break;

    		case 'approved':
    			 $status_pg= 1;
    			break;
    		
    		default:
    			$status_pg= 2;
    			break;
    	}

        $merchant_id= ($parametro['merchant_order_id'] != 'null')? $parametro['merchant_order_id'] : 0 ;

    	$data['statuspg']= $status_pg;
    	$data['merchant_order_id']= $merchant_id;
    	$data['tipo_pg']= $parametro['payment_type'];

    	$pedido= Pedido::where('id_mp_pedido',$parametro['preference_id'])->update($data);

        if($status_pg == 1){

            $pedido= Pedido::where('id_mp_pedido',$parametro['preference_id'])->first();

            $data= Userloja::findOrFail($pedido->userloja_id);

            $info = Info::findOrFail(1);

            $sendtoadm= $info->email;

            /* Enviar e-mail para o usuário com sua senha de acesso */
            Mail::to($sendtoadm)->send(new Avisocompra($data));

        }

    	return redirect()->route('painel.dash');


    }


}



