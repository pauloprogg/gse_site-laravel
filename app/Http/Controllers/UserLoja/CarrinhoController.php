<?php

namespace App\Http\Controllers\UserLoja;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Exception;
use MP;
use App\Pedido;

use Auth;

class CarrinhoController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth.loja');
    }

	 public function additemcarrinho(Request $request)
    {

        $up= $request->all();

        $data= date('d-m-Y', strtotime($up['data_agendamento']));

        $request->session()->push('carrinho', 
            [
                'titulo_servico'     => $up['titulo_servico'],
                'preco_servico' => $up['preco_servico'],
                'id_servico' => $up['id_servico'],
                'numero_profissional' => $up['numero_profissional'],
                'data_agendamento' => $data,
                'hora_agendamento' =>  $up['hora_agendamento'],
                'id_hora_agendamento' =>  $up['id_hora_agendamento'],
                'identificador' =>  $up['identificador'],
                'imagem_servico' => $up['imagem_servico'],
            ]
        );

        return redirect()->route('carrinho.painel');


    }

    public function ajaxcarrinho(Request $request, $id){

        $identificador= $id;

        $servicos_carrinho = session()->get('carrinho'); // Second argument is a default value

        $status= false;

        foreach ($servicos_carrinho as $key => $value) {

            if( $value['identificador'] == $identificador ){
                unset($servicos_carrinho[$key]);
                $status= true;
            }

        }
        session()->put('carrinho', $servicos_carrinho);
        // if(( array_search($itemcarrinho['identificador'], $servico_carrinho)) !== false) {
        //     unset($servico_carrinho[$key]);
        // }
        // session()->put('products', $products);


        //$value = $request->session()->pull('identificador', $itemcarrinho);
        

        return redirect()->route('carrinho.painel');

    }



    public function carrinho(Request $request)
    {

        $msg_finalizacao_compra= session()->get('msg_finalizacao_compra');

        $page= new \stdClass();
        $page->description= 'Você está no carrinho de compra';
        $page->keywords= 'go spa express, go spa, spa, serviços de spa';
        $page->title= 'Carrinho de Compra';
        $title = 'Carrinho de Compra';

        $carrinho = $request->session()->get('carrinho');

        $urlajax_delete=   $request->root().'/painel-do-cliente/carrinhoajax';

        //dd($carrinho);

        return view('painelcliente.carrinho', compact('page','title', 'carrinho', 'msg_finalizacao_compra', 'urlajax_delete'));

    }

    public function limparcarrinho()
    {

        session()->forget('carrinho');
        return redirect()->route('carrinho.painel');

    }


    /*
        Função que grava o pedido do usuário após retorno da função create_preference MercadoPago
    */

    public function gravapedido( $carrinho, $id_mp_pedido, $usuario )
    {

        $pedido[]= '';

        $status_gravar= false;

        foreach ($carrinho as $item) {

            $valor= str_replace(',', '.', str_replace('.', '', $item['preco_servico']));//Transforma o valor de real para decimal

            $data_agendamento= date('Y-m-d', strtotime($item['data_agendamento']));

            $pedido['userloja_id']= $usuario['id'];
            $pedido['titulo_produto']= $item['titulo_servico'];
            $pedido['price']= $valor;
            $pedido['produto_id']= $item['id_servico'];
            $pedido['quant_profissional']= $item['numero_profissional'];
            $pedido['data_agendamento']= $data_agendamento;
            $pedido['hora_agendamento']= $item['hora_agendamento'];
            $pedido['id_hora_agendamento']= $item['id_hora_agendamento'];
            $pedido['id_mp_pedido']= $id_mp_pedido;
            $pedido['img_servico']= $item['imagem_servico'];

            if(Pedido::create($pedido)){ $status_gravar= true; }else{ $status_gravar= false; }//Grava pedido no banco

        }

        if($status_gravar){
            session()->forget('carrinho');
        }

        return $status_gravar;//Retorna o status de gravação no banco


    }


    public function finalizarcompra(Request $request)
    {

    	$carrinho= session()->get('carrinho');//Instancia o carrinho de compra
    	if( !empty($carrinho) )
    	{

            $usuario= Auth::guard('loja')->user();//Instancia o usuario logado na loja

            $nome_user= str_word_count($usuario->name);

            if($nome_user != 1){
                list($nome, $sobrenome) = explode(' ', $usuario->name, 2);//Separa o primeiro nome dos sobrenomes
            }else{
               $nome= $usuario->name;
               $sobrenome= 'Sobrenome';
            }
            
            $telefone= explode(" ", $usuario->celular);//Separa o prefixo (15) do numero
            $telefone_ddd= str_replace(array('(',')'), array('',''), $telefone[0]);//Retira os parenteses do prefixo
            $telefone= $telefone[1];

    		$page= new \stdClass();
	        $page->description= 'Finalização da compra';
	        $page->keywords= 'go spa express, go spa, spa, serviços de spa';
	        $title = 'Finalização da compra - Go Spa';

	        $carrinho = $request->session()->get('carrinho');

            foreach ($carrinho as $item) {

                $valor= str_replace(',', '.', str_replace('.', '', $item['preco_servico']));//Transforma o valor de real para decimal
                $valor = (double) $valor;
                $descricao= $item['titulo_servico'].' - Agendado para o dia '.str_replace('-', '/', $item['data_agendamento']).' - Hora '.$item['hora_agendamento'];

                $itens[]= array(
                    "title" => $item['titulo_servico'],
                    "quantity" => 1,
                    "currency_id" => "BRL",
                    "picture_url" => $item['imagem_servico'],
                    "description" => $descricao,
                    "unit_price" => $valor
                );

            }

             

            $preference_data = array (
                "items" => $itens,
                "payment_methods" => array(
                    "excluded_payment_types" => array(
                        array(
                            "id" => "ticket"
                        )
                    ),
                    "installments" => 1,
                ),
                "payer" => array(
                    "name" => $nome,
                    "surname" => $sobrenome,
                    "email" => $usuario->email,
                    "phone" => array(
                        "area_code" => $telefone_ddd,
                        "number" => $telefone
                    )

                ),
                "back_urls" => array(
                    "success" => route('pagamento.status'),
                    "pending" => route('pagamento.status'),
                    "failure" => route('pagamento.status')
                ),
                "auto_return" => "approved"

            );

            //dd($preference_data);

            try {
                $preference = MP::create_preference($preference_data);
                $grava_pedido= self::gravapedido($carrinho, $preference['response']['id'], $usuario);//O self do laravel instancia outra função que se encontra no mesmo controlador
                if($grava_pedido){//Se true para pedidos gravados
                    //return redirect()->to($preference['response']['sandbox_init_point']);//Url para o sandbox mercadopago
                    return redirect()->to($preference['response']['init_point']);//Url para ambiente produção
                }else{
                    return redirect()->route('carrinho.painel')->with('msg_finalizacao_compra', 'Não foi Possivel realizar a compra, tente mais tarde ou entre em contato com o administrador do site!');
                }
                
            } catch (Exception $e){
                dd($e->getMessage());
            }

        
	        return view('painelcliente.finalizarcompra', compact('page','title', 'carrinho'));

    	}else{
    		return redirect()->route('home');
    	}

    }

    public function detalhespedido(Request $request)
    {
        
    }

}
