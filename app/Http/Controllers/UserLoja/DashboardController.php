<?php

namespace App\Http\Controllers\UserLoja;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserslojaRequest;
use Illuminate\Support\Facades\Session;

use Auth;
use App\Userloja;
use App\Post;
use App\Pedido;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.loja');
    }

    public function index(Request $request)
    {

        $usuario= Auth::guard('loja')->user();//Instancia o usuario logado na loja

        $pedidos= Pedido::where('userloja_id', $usuario->id)->orderBy('data_agendamento', 'DESC')->get();

    	$page= new \stdClass();
        $page->description= 'Você está no painel do cliente';
        $page->keywords= 'go spa express, go spa, spa, serviços de spa';
        $page->title = 'Painel do Cliente';

        return view('painelcliente.dashboard', compact('title', 'page', 'pedidos'));
    }



    public function ajaxagendamento(Request $request)
    {
        $post= $request->all();

        $agenda= Pedido::where('data_agendamento',$post['data_agenda'])
        ->where('statuspg',1)
        ->where('produto_id',$post['servico_id'])
        ->get();

        return json_encode($agenda);

    }

}
