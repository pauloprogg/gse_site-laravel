<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use Auth;

class LoginlojaController extends Controller
{
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/painel-do-cliente/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:loja')->except('logout');
    }

    public function loginentrar(Request $request)
    {

      // Validate the form data
      $this->validate($request, [
        'email'   => 'required',
        'password' => 'required'
      ]);


      // Attempt to log the user in
      if (Auth::guard('loja')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
        // if successful, then redirect to their intended location
        return redirect()->intended(route('painel.dash'));
      }

      // if unsuccessful, then redirect back to the login with the form data
      Session::flash('message', 'Usuário e/ou senha inválido(s)!');
      Session::flash('class', 'danger');
      return redirect()->back()->withInput($request->only('email', 'remember'));
    }

    /* Sobrepor a função de levar até a página de login */
    public function showLoginForm()
    {

        $msg_cadastro= session()->get('msg_cadastro');

        $logado=true;
        $page= new \stdClass();
        $page->description= 'Pagina de login para o painel do cliente';
        $page->keywords= 'go spa express, go spa, spa, serviços de spa';

        $page->title = "Faça o login para continuar";

        return view('painelcliente.auth.login', compact('page', 'title', 'logado', 'msg_cadastro'));
    }

    public function logout(Request $request)
    {
      $request->session()->flush();
      Auth::guard('loja')->logout();
      return redirect()->route('loja.login');
    }
}