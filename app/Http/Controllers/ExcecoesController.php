<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PostsRequest;
use Illuminate\Support\Facades\Session;

use App\Post;
use App\Excecao;

class ExcecoesController extends Controller
{
    
	/**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
	public function index(Request $request)
    {

        /* 1 = Administrador | 2 = Cliente | 3 = Conteudoria | 4 = Editor | 5 = Redator */
        if(!$request->user()->authorizeRoles(['1'])) {
            $title = "Acesso não autorizado";
            return view('cms.errors.401', compact('title'));
        }

        $excecoes = Excecao::orderBy('id', 'DESC')->paginate(10);
        $title = 'Listagem de Exceções';

        return view('cms.excecoes.index', compact('title', 'excecoes'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        /* 1 = Administrador | 2 = Cliente | 3 = Conteudoria | 4 = Editor | 5 = Redator */
        if(!$request->user()->authorizeRoles(['1'])) {
            $title = "Acesso não autorizado";
            return view('cms.errors.401', compact('title'));
        }

        $servicos = Post::
        select('id','title')
        ->get();

        $title = 'Inserindo nova exceção para um serviço';

        return view('cms.excecoes.create', compact('title','servicos'));
    }



    public function store(Request $request)
    {
        $new = $request->all();

        $data = str_replace("/", "-", $new['data']);
    	$new['data']=date('Y-m-d', strtotime($data));

        Excecao::create($new);

        Session::flash('message', 'Adicionado com sucesso!');
        Session::flash('class', 'success');
        return redirect()->route('excecoes.index');
    }

    public function destroy(Request $request, $id)
    {
        /* 1 = Administrador | 2 = Cliente | 3 = Conteudoria | 4 = Editor | 5 = Redator */
        if(!$request->user()->authorizeRoles(['1'])) {
            $title = "Acesso não autorizado";
            return view('cms.errors.401', compact('title'));
        }

        $excecao = Excecao::findOrFail($id);

        $excecao->delete();

        Session::flash('message', 'Removido com sucesso!');
        Session::flash('class', 'danger');
        return redirect()->route('excecoes.index');
    }


}
