<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserslojaRequest;
use Illuminate\Support\Facades\Session;

use App\Userloja;
use App\File;


class UserslojaController extends Controller
{
    public function index(Request $request)
    {
        /* 1 = Administrador | 2 = Cliente | 3 = Conteudoria | 4 = Editor | 5 = Redator */

        if($request->user()->authorizeRoles(['1'])) {
            $usuariosloja = Userloja::orderBy('id', 'DESC')->paginate(10);
            $title = "Listando Usuários da loja";

            return view('cms.usuariosloja.index', compact('title', 'usuariosloja'));

        } else {
            $title = "Acesso não autorizado";
            return view('cms.errors.401', compact('title'));
        }
    }

    public function create(Request $request)
    {
        /* 1 = Administrador | 2 = Cliente | 3 = Conteudoria | 4 = Editor | 5 = Redator */
        if($request->user()->authorizeRoles(['1'])) {
            $title = "Novo usuário";

            return view('cms.usuariosloja.create', compact('title', 'roles'));

        } else {
            $title = "Acesso não autorizado";
            return view('cms.errors.401', compact('title'));
        }
    }

    public function store(UserslojaRequest $request)
    {
        $new = $request->all();

        $sendto = $new['email'];

        if($request->hasFile('image')){
            $file = $request->file('image');
            $file_name = time().'-'.$file->getClientOriginalName();
            $file_path = 'uploads/usersloja/';

            $file->move($file_path, $file_name);

            if($new['image'] != "") {
                $new['image'] = $file_name;
            }
        }

        /* Adicionar dados do cadastro em array, resgatando a senha antes de ser encriptada */
        $data = [
            'title'=> 'sua conta foi criada!',
            'name'=> $new['name'],
            'email'=> $new['email'],
            'password'=> $new['password']
        ];

        $new['password'] = bcrypt($request->password);

        Userloja::create($new);

        Session::flash('message', 'Adicionado com sucesso!');
        Session::flash('class', 'success');
        return redirect()->route('usuariosloja.index');
    }

    public function show($id)
    {
        //
    }

    public function edit(Request $request, $id)
    {

        $usuario = Userloja::findOrFail($id);

        $datanascimentoArray= explode('-', $usuario->datanascimento);

        /* 1 = Administrador | 2 = Cliente | 3 = Conteudoria | 4 = Editor | 5 = Redator */
        if($request->user()->authorizeRoles(['1'])) {
            $title = "Editando: ".$usuario->name;

            return view('cms.usuariosloja.edit', compact('title', 'usuario', 'datanascimentoArray'));

        } else {
            $title = "Acesso não autorizado";
            return view('cms.errors.401', compact('title'));
        }
    }

    public function update(UserslojaRequest $request, $id)
    {
        $usuario = Userloja::findOrFail($id);
        $up = $request->all();

         if($request->file('image')){

            $filename = pathinfo($usuario->image, PATHINFO_FILENAME);
            $extension = pathinfo($usuario->image, PATHINFO_EXTENSION);

            $nomearquivo= $filename.'.'.$extension;

            //dd($nomearquivo);

            $usersImage = public_path().'/uploads/usersloja/'.$nomearquivo; // get previous image from folder


            if (file_exists($usersImage)) { // unlink or remove previous image from folder
                unlink($usersImage);
            }

            $file = $request->file('image');
            $file_name = time().'-'.$file->getClientOriginalName();
            $file_path = 'uploads/usersloja/';

            $file->move($file_path, $file_name);

            if($up['image'] != "") {
                $up['image'] = $file_name;
            }

        }

        if($request->password != "") {
            $up['password'] = bcrypt($request->password);
        } else {
            $up['password'] = $usuario->password;
        }

        $usuario->update($up);

        Session::flash('message', 'Editado com sucesso!');
        Session::flash('class', 'success');
        return redirect()->route('usuariosloja.edit', $id);
    }

    public function updatepainel(Request $request)
    {
        
        $up = $request->all();

        $usuario = Userloja::findOrFail($up['user_id']);
        
         if($request->hasfile('image')){

            $filename = pathinfo($usuario->image, PATHINFO_FILENAME);
            $extension = pathinfo($usuario->image, PATHINFO_EXTENSION);

            $nomearquivo= $filename.'.'.$extension;

            $usersImage = public_path().'/uploads/usersloja/'.$nomearquivo; // get previous image from folder

            $file = $request->file('image');
            $file_name = time().'-'.$file->getClientOriginalName();
            $file_path = 'uploads/usersloja/';

            $file->move($file_path, $file_name);

            if($up['image'] != "") {
                $up['image'] = $file_name;
            }

            if (file_exists($usersImage)) { // unlink or remove previous image from folder
                unlink($usersImage);
            }
            

        }

        if($request->password != "") {
            $up['password'] = bcrypt($request->password);
        } else {
            $up['password'] = $usuario->password;
        }

        $usuario->update($up);

        Session::flash('message', 'Editado com sucesso!');
        Session::flash('class', 'success');
        return redirect()->route('painel.dash');
    }

    public function destroy(Request $request, $id)
    {
        /* 1 = Administrador | 2 = Cliente | 3 = Conteudoria | 4 = Editor | 5 = Redator */

        if($request->user()->authorizeRoles(['1','2'])) {
            $usuario = Userloja::findOrFail($id);

            $usuario->delete();

            Session::flash('message', 'Removido com sucesso!');
            Session::flash('class', 'danger');
            return redirect()->route('usuariosloja.index');

        } else {
            $title = "Acesso não autorizado";
            return redirect('cms.errors.401', compact('title'));
        }
    }
}
