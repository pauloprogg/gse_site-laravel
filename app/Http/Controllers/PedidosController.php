<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pedido;
use App\Userloja;


class PedidosController extends Controller
{
     public function index(Request $request)
    {
        /* 1 = Administrador | 2 = Cliente | 3 = Conteudoria | 4 = Editor | 5 = Redator */
        if(!$request->user()->authorizeRoles(['1','2','3','5'])) {
            $title = "Acesso não autorizado";
            return view('cms.errors.401', compact('title'));
        }

        $busca= ( $request->busca )? $request->busca : '';
        
        $pedidos = Pedido::when($request->busca, function($query) use ($request){

           return $query->where('userlojas.name', 'LIKE', '%'.$request->busca.'%')
           ->join('userlojas', 'userlojas.id', '=', 'pedidos.userloja_id');

       })->orderBy('pedidos.data_agendamento', 'DESC')
        ->paginate(20);

        $title = "Listando Pedidos";

        return view('cms.pedidos.index', compact('title', 'pedidos', 'busca'));
    }

}
