<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserslojaRequest;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use App\Mail\Cadastroloja;
use App\Mail\AvisocadastroForm;

use App\Userloja;
use App\Info;

class FormsController extends Controller
{

    public function index(Request $request)
    {

        $page= new \stdClass();
        $page->description= 'Pagina de cadastro - Go Spa Express';
        $page->keywords= 'o spa, go spa express, spa, cadastro go spa, pagina de cadastro';

        $page->title = "Faça o cadastro em nossa loja";

       return view('cadastro', compact('page'));
    }


    public function cadastro(UserslojaRequest $request){

    	$new= $request->all();

        if($new['politicas']=="on"){
            $new['politicas']= 1;
        };

    	 /* Adicionar dados do cadastro em array, resgatando a senha antes de ser encriptada */
        $data = [
            'name'=> $new['name'],
            'email'=> $new['email'],
            'telefone'=> $new['telefone'],
            'celular'=> $new['celular'],
            'datanascimento'=> $new['datanascimento'],
            'sexo'=> $new['sexo'],
            'password'=> $new['password'],
            'politicas'=> $new['politicas']
        ];

        $sendto = $new['email'];

        $new['password'] = bcrypt($request->password);//Encryptografa a senha para gravar no banco

        /* Enviar e-mail para o usuário com sua senha de acesso */
        Mail::to($sendto)->send(new Cadastroloja($data));

        Userloja::create($new);

        $info = Info::findOrFail(1);

        $sendtoadm= $info->email;

        /* Enviar e-mail para o usuário com sua senha de acesso */
        Mail::to($sendtoadm)->send(new AvisocadastroForm($data));

        return redirect()->route('loja.login')->with('msg_cadastro', 'Cadastrado com sucesso! Realize login para acessar seu painel do cliente.');

    }



}
