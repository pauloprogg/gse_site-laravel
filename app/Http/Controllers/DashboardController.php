<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Post;
use App\Page;
use App\Pedido;

class DashboardController extends Controller
{
    public function index()
    {

    	
    	$pages = Page::all();
    	$posts = Post::limit(5)->orderBy('id', 'desc')->get();
    	$pedidos = Pedido::where('data_agendamento', '>=', date('Y-m-d'))
    	->where('data_agendamento', '<=', date('Y-m-d', strtotime(date('Y-m-d') . ' +7 day')))
    	->get();
    	
    	$usuarios = User::all();
        $title = 'Dashboard';
        return view('cms.dashboard', compact('title', 'pages', 'posts', 'usuarios', 'pedidos'));
    }

}
