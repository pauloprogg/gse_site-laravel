<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class PagesRequest extends FormRequest
{
    /**
     * Verifica se a validação é verdadeira ou falsa
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Array de campos obrigatórios do conteúdo
     *
     * @return array
     */
    public function rules(Request $request)
    {

        switch ($request->page) {
            case 1:

                return [
                    'title' => 'required|max:60',
                    'name' => 'required'
                ];

                break;
            
            default:

                return [
                    'title' => 'required|max:60',
                    'name' => 'required',
                    'content' => 'required'
                ];

                break;
        }

        // return [
        //     'title' => 'required|max:20',
        //     'name' => 'required',
        //     'content' => 'required'
        // ];

        
    }

    /**
     * Array de mensagens personalizadas dos campos obrigatórios
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'Título obrigatório',
            'name.required' => 'Digite o nome da página',
            'content.required' => 'Digite o conteúdo da página'
        ];
    }
}