<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContatoFormRequest extends FormRequest
{
    /**
      * Verifica se a validação é verdadeira ou falsa
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
    * Array de campos obrigatórios do conteúdo
     *
     * @return array
     */
    public function rules()
    {
        return [
                'name' => 'required',
                'email' => 'required | email',
                'assunto' => 'required',
                'mensagem' => 'required'
        ];
    }


     /**
     * Array de mensagens personalizadas dos campos obrigatórios
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nome.required' => 'Nome obrigatório',
            'email.required' => 'Digite um e-mail válido',
            'assunto.required' => 'Assunto obrigatório',
            'mensagem.required' => 'Digite uma mensagem'
        ];
    }



}
