<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostsRequest extends FormRequest
{
    /**
     * Verifica se a validação é verdadeira ou falsa
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Array de campos obrigatórios do conteúdo
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required',
            'categoria_id'=>'required',
            'image'=> 'dimensions:max_width=600,max_height=600',
            'content'=> 'max:400'
        ];
    }

    /**
     * Array de mensagens personalizadas dos campos obrigatórios
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required'=>'Título obrigatório!',
            'categoria_id.required'=>'Escolha uma categoria!',
            'image'=>'Tamanho excedido, insira uma imagem de até 600 x 600!',
            'content'=>'Tamanho maximo de caracteres excedido!'
        ];
    }




}
