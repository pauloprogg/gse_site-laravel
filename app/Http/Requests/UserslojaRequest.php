<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class UserslojaRequest extends FormRequest
{
   /**
     * Verifica se a validação é verdadeira ou falsa
    *
    * @return bool
    */
   public function authorize()
   {
       return true;
   }

   /**
    * Array de campos obrigatórios do conteúdo
    *
    * @return array
    */
   public function rules(Request $request)
   {


      $required= [
            'name'=>'required',
            'email'=>'required',
            'celular'=>'required',
            'image'=> 'dimensions:max_width=600'
       ];

      if($request->segment(3) == NULL){
        $required['password']= "required|confirmed";
      } else {
        $required['password']= "confirmed";
      }

      return $required;

   }


   /**
    * Array de mensagens personalizadas dos campos obrigatórios
    *
    * @return array
    */
   public function messages()
   {
       return [
           'name.required'=>'Nome obrigatório!',
            'email.required'=>'Email obrigatório',
            'celular'=>'Celular é obrigatório',
            'image'=>'Tamanho excedido, insira uma imagem de até 600 x 600!',
            'password.required' => 'Senha obrigatória',
            'password.confirmed' => 'A senha e confirmação de senha não conferem'
       ];
   }

}
