<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProfissionaisRequest;
use Illuminate\Support\Facades\Session;

use App\Profissional;

class CategoriasController extends Controller
{
    public function index(Request $request)
    {
        /* 1 = Administrador | 2 = Cliente | 3 = Conteudoria | 4 = Editor | 5 = Redator */
        if(!$request->user()->authorizeRoles(['1','2','3','5'])) {
            $title = "Acesso não autorizado";
            return view('cms.errors.401', compact('title'));
        }
        
        $profissionais = Profissional::orderBy('id', 'DESC')->paginate(5);
        $title = "Listando Profissionais";

        return view('cms.profissionais.index', compact('title', 'profissionais'));
    }

    public function create(Request $request)
    {
        /* 1 = Administrador | 2 = Cliente | 3 = Conteudoria | 4 = Editor | 5 = Redator */
        if(!$request->user()->authorizeRoles(['1','2','3','5'])) {
            $title = "Acesso não autorizado";
            return view('cms.errors.401', compact('title'));
        }
        
        $title = "Novo profissional";

        return view('cms.profissionais.create', compact('title'));
    }

    public function store(ProfissionaisRequest $request)
    {
        $new = $request->all();
        Profissional::create($new);

        Session::flash('message', 'Adicionado com sucesso!');
        Session::flash('class', 'success');
        return redirect()->route('profissionais.index');
    }

    public function show($id)
    {
        //
    }

    public function edit(Request $request, $id)
    {
        /* 1 = Administrador | 2 = Cliente | 3 = Conteudoria | 4 = Editor | 5 = Redator */
        if(!$request->user()->authorizeRoles(['1','2','3','5'])) {
            $title = "Acesso não autorizado";
            return view('cms.errors.401', compact('title'));
        }
        
        $categoria = Profissional::findOrFail($id);
        $title = "Editando: ".$categoria->label;

        return view('cms.profissionais.edit', compact('title', 'categoria'));
    }

    public function update(ProfissionaisRequest $request, $id)
    {
        $categoria = Profissional::findOrFail($id);
        $up = $request->all();
        $categoria->update($up);

        Session::flash('message', 'Editado com sucesso!');
        Session::flash('class', 'success');
        return redirect()->route('profissionais.edit', $id);
    }

    public function destroy(Request $request, $id)
    {
        /* 1 = Administrador | 2 = Cliente | 3 = Conteudoria | 4 = Editor | 5 = Redator */
        if(!$request->user()->authorizeRoles(['1','2','3','5'])) {
            $title = "Acesso não autorizado";
            return view('cms.errors.401', compact('title'));
        }
        
        $categoria = Profissional::findOrFail($id);

        $categoria->delete();

        Session::flash('message', 'Removido com sucesso!');
        Session::flash('class', 'danger');
        return redirect()->route('profissionais.index');
    }
}
