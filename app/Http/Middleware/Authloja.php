<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class AuthLoja
{
   public function handle($request, Closure $next)
   {
       if (Auth::guard('loja')->check()) {
           return $next($request);
       }

       return redirect()->route('loja.login');
   }
}