<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profissional extends Model
{
    protected $table = 'profissionais';
    protected $fillable = [
    	'nome', 'perfilimg'
    ];

    /* Função para adicionar a URL do site automaticamente na imagem após puxar do banco
    URL determinada no .env */
    public function getPerfilimgAttribute($value) {
        if($value) {
            return config('app.url').'uploads/profissionais/'.$value;
        }
    }
}
