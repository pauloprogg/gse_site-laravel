<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Post extends Model
{

    use Sluggable;

   /**
   * Transforma o titulo do registro em slug
   *
   * @return array
   */

   public function sluggable()
   {
       return [
           'slug' => [
               'source' => 'title',
               'onUpdate' => true
           ]
       ];
   }

	/* Array de campos protegidos a serem gravados no banco. Ao usar [body] ele aceitará TODOS os campos. */
    protected $fillable = [
        'title', 'content', 'summary', 'image', 'status', 'categoria_id', 'profissional_id', 'numero_profissional', 'tempo_atendimento', 'price', 'agendamento', 'keywords', 'description'
    ];

    /* Função para adicionar a URL do site automaticamente na imagem após puxar do banco
    URL determinada no .env */
    public function getImageAttribute($value) {
        if($value) {
            return config('app.url').'uploads/servicos/'.$value;
        }
    }

    /* Atribuir POST a uma CATEGORIA. (Posts são pertencentes a uma categoria - belongsTo) */
    public function categoria(){
        return $this->belongsTo('App\Categoria');
    }

     /* Atribuir POST a uma PROFISSIONAL. (Posts são pertencentes a um profissional - belongsTo) */
    public function profissional(){
        return $this->belongsTo('App\Profissional');
    }

    /* Retorna as exceções relacionadas a um POST */
    public function excecoes(){
        return $this->hasMany('App\Excecao', 'post_id');
    }

}
