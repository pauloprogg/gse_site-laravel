<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Categoria extends Model
{

	use Sluggable;

   /**
   * Transforma o titulo do registro em slug
   *
   * @return array
   */

   public function sluggable()
   {
       return [
           'slug' => [
               'source' => 'label',
               'onUpdate' => true
           ]
       ];
   }

	/* Array de campos protegidos a serem gravados no banco. Ao usar [body] ele aceitará TODOS os campos. */
    protected $fillable = [
    	'label', 'content', 'slug'
    ];
}
