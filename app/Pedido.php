<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    /* Array de campos protegidos a serem gravados no banco. Ao usar [body] ele aceitará TODOS os campos. */
    protected $fillable = [
    	'userloja_id','titulo_produto', 'price', 'produto_id', 'quant_profissional', 'data_agendamento', 'hora_agendamento', 'id_hora_agendamento', 'id_mp_pedido', 'img_servico', 'statuspg', 'merchant_order_id', 'tipo_pg'
    ];


    /* Atribuir Pedido a um usuário. (Pedido pertence a um usuário - belongsTo) */
    public function usuarioloja(){
        return $this->belongsTo('App\Userloja','userloja_id');
    }


}
