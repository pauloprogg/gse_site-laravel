<?php

use Faker\Generator as Faker;

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => 'Alex Alves',
        'role_id' => '1',
        'image' => NULL,
        'email' => 'danilo@sorocabacom.com',
        'email_verified_at' => NULL,
        'password' => bcrypt('asdf'),
        'remember_token' => '',
    ];
});
