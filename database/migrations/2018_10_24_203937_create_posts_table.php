<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if(!Schema::hasTable('posts')){

            Schema::create('posts', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('status');
                $table->string('title');
                $table->string('slug');
                $table->string('summary')->nullable();
                $table->text('agendamento')->nullable();
                $table->text('content')->nullable();
                $table->integer('categoria_id');
                $table->integer('tempo_atendimento')->nullable();
                $table->integer('numero_profissional')->nullable();
                $table->integer('profissional_id')->nullable();
                $table->string('image')->nullable();
                $table->decimal('price', 8,2)->nullable();
                $table->string('keywords')->nullable();
                $table->string('description')->nullable();
                $table->timestamps();
            });
        }else if(!Schema::hasColumn('posts', 'price')){
            Schema::table('posts', function (Blueprint $table)
            {
                //$table->string('agendamento');
            });
            
        }

        
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
