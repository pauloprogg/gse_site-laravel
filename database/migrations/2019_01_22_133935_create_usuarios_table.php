<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     * Tabela que armazena os usuários da loja
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('usuarios')){
            Schema::create('usuarios', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->integer('status')->default('1');
                $table->integer('sexo')->default('1');
                $table->string('image')->nullable();
                $table->string('telefone')->nullable();
                $table->string('celular')->nullable();
                $table->date('datanascimento');
                $table->string('email')->unique();
                $table->timestamp('email_verified_at')->nullable();
                $table->string('password');
                $table->rememberToken();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
