<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userloja_id');
            $table->string('titulo_produto');
            $table->decimal('price', 8,2)->nullable();
            $table->integer('produto_id');
            $table->integer('quant_profissional');
            $table->date('data_agendamento');
            $table->string('hora_agendamento');
            $table->string('id_hora_agendamento');
            $table->string('id_mp_pedido');
            $table->string('img_servico');
            $table->integer('statuspg')->default('0')->comment('0 para em processo, 1 para aprovado e 2 para recusado');
            $table->integer('merchant_order_id')->nullable();
            $table->string('tipo_pg')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidos');
    }
}
