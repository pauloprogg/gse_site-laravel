<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if(!Schema::hasTable('infos')){

            Schema::create('infos', function (Blueprint $table) {
                $table->increments('id');
                $table->text('scriptshead')->nullable();
                $table->text('scriptsfoot')->nullable();
                $table->string('facebook')->nullable();
                $table->string('instagram')->nullable();
                $table->string('telefone1')->nullable();
                $table->string('telefone2')->nullable();
                $table->string('email')->nullable(); 
                $table->string('footer')->nullable(); 
                $table->string('copyright')->nullable();
                $table->text('endereco')->nullable();
                $table->timestamps();
            });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('infos');
    }
}
