<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if(!Schema::hasTable('slides')){

            Schema::create('slides', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('status');
                $table->string('image');
                $table->string('title');
                $table->text('summary')->nullable();
                $table->string('button');
                $table->string('url');
                $table->integer('target');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slides');
    }
}
