<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if(!Schema::hasTable('pages')){

            Schema::create('pages', function (Blueprint $table) {
                $table->increments('id');
                $table->string('title')->nullable();
                $table->string('name');
                $table->string('frase')->nullable();
                $table->text('content')->nullable();
                $table->text('summary')->nullable();
                $table->string('headimg')->nullable();
                $table->string('headtitle')->nullable();
                $table->integer('galeria_id')->nullable();
                $table->string('destaquehomeimg')->nullable();
                $table->string('destaquehomeserv1')->nullable();
                $table->string('destaquehomeserv2')->nullable();
                $table->string('destaquehomeserv3')->nullable();
                $table->string('keywords')->nullable();
                $table->string('description')->nullable();
                $table->timestamps();
            });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
