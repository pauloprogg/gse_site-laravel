<?php

use Illuminate\Database\Seeder;

class PagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pages = [
            ['id' => 1, 'name' => 'Inicial', 'title' => 'Pagina Inicial'],
            ['id' => 2, 'name' => 'Conheça', 'title' => 'Sobre nós'],
            ['id' => 3, 'name' => 'Serviços', 'title' => 'Nossos Serviços'],
            ['id' => 4, 'name' => 'Cadastro', 'title' => 'Pagina de Cadastro'],
            ['id' => 5, 'name' => 'Compra', 'title' => 'Pagina de Compra'],
            ['id' => 6, 'name' => 'Cancelamento', 'title' => 'Pagina sobre as políticas de compra'],
            ['id' => 7, 'name' => 'Contato', 'title' => 'Página de Contato'],
            ['id' => 8, 'name' => 'Serviço', 'title' => 'Página de Serviço'],
            ['id' => 9, 'name' => 'Políticas de Privacidade', 'title' => 'Página de Termos e Políticas']
        ];

        DB::table('pages')->insert($pages);
    }
}


