<?php

/* ROUTES AUTHENTICATE */
Route::group(['prefix'=>'painel-do-cliente'], function(){
	Route::get('/entrar', 'LoginlojaController@showLoginForm')->name('loja.login');
	Route::post('/entrar/validar', 'LoginlojaController@loginentrar')->name('loja.validate');
	Route::get('/sair', 'LoginlojaController@logout')->name('loja.logout');
});

/* PAINEL */
Route::group(['prefix'=>'painel-do-cliente','middleware' => 'auth.loja'], function(){

	Route::get('/', 'UserLoja\DashboardController@index')->name('painel.dash');
	Route::post('/userpainel', 'UserslojaController@updatepainel')->name('userpainel.update');

	Route::post('/addcarrinho', 'UserLoja\CarrinhoController@additemcarrinho')->name('addcarrinho.painel');
	Route::get('/carrinho', 'UserLoja\CarrinhoController@carrinho')->name('carrinho.painel');
	Route::get('/carrinho/limpar', 'UserLoja\CarrinhoController@limparcarrinho')->name('carrinho.limpar');
	Route::get('/carrinho/finalizarcompra', 'UserLoja\CarrinhoController@finalizarcompra')->name('finalizar.compra');

	Route::get('/pedido/ver/id/{id}', 'UserLoja\DashboardController@detalhespedido')->name('pedido.ver');

	//Route::get('ajax', function(){ return view('ajax'); });
	//Route::get('/carrinhoajax','UserLoja\CarrinhoController@ajaxcarrinho')->name('pedido.delete');
	Route::get('/carrinhoajax/{id}','UserLoja\CarrinhoController@ajaxcarrinho')->name('pedido.delete');
	
	Route::post('/ajaxagendamento','UserLoja\DashboardController@ajaxagendamento')->name('checa.agenda');
});

