<?php

/* ROUTES SITE */
Route::get('/','PagesController@home')->name('home');// Recupera as informações da função em PagesController.php
Route::get('/conheca','PagesController@conheca')->name('conheca');// Recupera as informações da função em PagesController.php
Route::get('servicos','PagesController@servicos')->name('servicos');// Recupera as informações da função em PagesController.php
Route::get('/contato','PagesController@contato')->name('contato');// Recupera as informações da função em PagesController.php
Route::get('/servico/{slug}','PagesController@servico')->name('servico');
Route::get('politicasdeprivacidade','PagesController@privacidade')->name('politicasdeprivacidade');// Recupera as informações da função em PagesController.php
Route::get('compra','PagesController@compra')->name('compra');// Recupera as informações da função em PagesController.php
Route::get('cancelamento','PagesController@cancelamento')->name('cancelamento');// Recupera as informações da função em PagesController.php

Route::get('/cadastro','FormsController@index')->name('cadastro');// Recupera as informações da função em PagesController.php
Route::post('/cadastro/inserir', 'FormsController@cadastro')->name('userloja.cadastro');
Route::post('/contato/enviar', 'PagesController@contatoenviar')->name('contato.enviar');

Route::get('/pagamento/status', 'RetornopagamentoController@pagamentostatus')->name('pagamento.status');






/* ROUTES AUTHENTICATE */
Route::group(['prefix'=>'cms'], function(){
	Auth::routes();
});

/* ROUTES CMS */
Route::group(['prefix'=>'cms','middleware' => 'auth'], function(){
	Route::get('/', 'DashboardController@index')->name('dashboard');
	Route::get('dashboard', 'DashboardController@index')->name('dashboard');

	Route::post('/busca', 'PedidosController@index')->name('pedidos.busca');
	Route::get('/busca', 'PedidosController@index')->name('pedidos.busca');

	//Static Pages
	Route::resource('pages', 'PagesController');
	Route::resource('informacoes', 'InformacoesController');

	//Dynamic Content
	Route::resource('posts', 'PostsController');
	Route::resource('categorias', 'CategoriasController');
	Route::resource('galerias', 'GaleriasController');
	Route::resource('usuarios', 'UsersController');
	Route::resource('usuariosloja', 'UserslojaController');
	Route::resource('profissionais', 'ProfissionaisController');
	Route::resource('pedidos', 'PedidosController');
	Route::resource('slides', 'SlidesController');
	Route::resource('excecoes', 'ExcecoesController');
	
	//Medias content
	Route::get('galerias/files/{id}', 'GaleriasController@filesall')->name('files.all');
	Route::post('galerias/files/up/{id}', 'GaleriasController@fileup')->name('file.up');
	Route::post('galerias/files/del/{id}', 'GaleriasController@filedel')->name('file.del');
});