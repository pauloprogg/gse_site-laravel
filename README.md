## Como utilizar
- 1) Renomeie o arquivo ".env.example" para ".env" e configure a URL, MYSQL e MAILGUN (opcional) com os dados corretos.
- 2) No terminal digite (sem aspas): "composer install" - Para que seja instalado todas as dependências do LARAVEL
- 3) Após instalar as dependências do LARAVEL, digite no terminal (sem aspas): "php artisan key:generate" - Para gerar sua chave de aplicação parar encriptar seus dados.
- 4) No terminal digite (sem aspas): "npm install" - Para que seja instalado todas as dependências do WEBPACK
- 5) No terminal digite (sem aspas): "php artisan migrate --seed" - Para que o sistema crie todas as tabelas e colunas do banco de dados e popule com informações fake de acordo com as Migrations criadas.
- 6) Para rodar o webpack: "npm run watch" - Para monitorar alterações. "npm run production" - Para compilar e minificar para produção.

Obs.: É necessário que o seu ambiente de produção tenha o PHP 7.2 configurado. (Para usuários Windows, utilize o Wamp)

## Regras de utilização do GIT
- 1) Antes de qualquer alteração, verifique se há diferença entre local e repositório (git status).
- 2) Utilize apenas sua branch (git checkout -b idtarefa_dev) para atualizações, nunca a MASTER. Caso aconteça, dê um rollback e refaça a ação com a branch correta.
- 3) Não esqueça de comentar (git commit -m "sua mensagem entre aspas") de forma coerente, detalhada e de fácil entendimento.
- 4) Todas as branches deverão dar MERGE na DEV, pois é a branch de homologação. O merge na master deverá ser feito apenas após o site ser entregue.
- 5) Futuras correções de bugs deverão ter uma branch nova com o tipo: Hotfix. Respeitando a regra do nome "idrunrun_dev".
- 6) Futuras adições ao site deverão ter uma branch nova com o tipo: Feature. Respeitando a regra do nome "idrunrun_dev".

## Sobre Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, yet powerful, providing tools needed for large, robust applications.

## Aprendendo Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of any modern web application framework, making it a breeze to get started learning the framework.

If you're not in the mood to read, [Laracasts](https://laracasts.com) contains over 1100 video tutorials on a range of topics including Laravel, modern PHP, unit testing, JavaScript, and more. Boost the skill level of yourself and your entire team by digging into our comprehensive video library.

## Segurança

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).